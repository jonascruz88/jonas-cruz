import org.junit.Test;
import static org.junit.Assert.*;

public class PersonagemTest {

	@Test(expected = IndexOutOfBoundsException.class)
	public void testaGanharItem() {
		Personagem legolas = new Elfo("Legolas");
		Item casaco = new Item("Casaco", 2);
		Item chave = new Item("Chave", 3);
		Item garfo = new Item("Garfo", 1);
		Item faca = new Item("Faca", 1);
		Item pocao = new Item("Pocao de HP", 4);
		legolas.ganharItem(garfo);
		legolas.ganharItem(faca);
		legolas.ganharItem(chave);
		legolas.ganharItem(casaco);
		assertEquals(garfo, legolas.getItem(2));
		assertEquals(faca, legolas.getItem(3));
		assertEquals(casaco, legolas.getItem(5));
		assertEquals(chave, legolas.getItem(4));
		assertEquals(pocao, legolas.getItem(6));

	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testaPerderItem() {
		Personagem legolas = new Elfo("Legolas");
		Item casaco = new Item("Casaco", 2);
		Item chave = new Item("Chave", 3);
		Item garfo = new Item("Garfo", 1);
		Item faca = new Item("Faca", 1);
		legolas.ganharItem(garfo);
		legolas.ganharItem(faca);
		legolas.ganharItem(chave);
		legolas.ganharItem(casaco);
		legolas.perderItem(garfo);
		assertNotSame(garfo, legolas.getItem(2));
		assertEquals(faca, legolas.getItem(2));
		assertEquals(casaco, legolas.getItem(4));
		assertEquals(chave, legolas.getItem(3));
		assertEquals(garfo, legolas.getItem(5));

	}

	@Test
	public void testaGetInventario() {
		Personagem legolas = new Elfo("Legolas");
		Inventario invent = legolas.getInventario();
		boolean comparacao = invent.equals(legolas.getInventario());
		assertEquals(true, comparacao);

	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testElfoVerdeGanharItemExperiencia() {
		Elfo legolas = new ElfoVerde("Legolas");
		Dwarf inimigo = new Dwarf("Inimigo");
		Item flechaDeVidro = new Item("Flecha de Vidro", 2);
		Item chave = new Item("Chave", 3);
		Item garfo = new Item("Garfo", 1);
		Item faca = new Item("Faca", 1);
		legolas.ganharItem(garfo);
		legolas.ganharItem(faca);
		legolas.ganharItem(chave);
		legolas.ganharItem(flechaDeVidro);
		legolas.perderItem(garfo);
		legolas.atirarFlecha(inimigo);
		legolas.atirarFlecha(inimigo);
		assertNotSame(garfo, legolas.getItem(0));
		assertNotSame(faca, legolas.getItem(1));
		assertEquals(flechaDeVidro, legolas.getItem(2));
		assertEquals("Chave", legolas.getItem(3).getNome());
		assertEquals(4, legolas.getExperiencia());

	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testElfoVerdePerderItem() {
		Personagem legolas = new ElfoVerde("Legolas");
		Item flechaDeVidro = new Item("Flecha de Vidro", 2);
		Item arcoDeVidro = new Item("Arco de Vidro", 2);
		Item chave = new Item("Chave", 3);
		Item garfo = new Item("Garfo", 1);
		Item faca = new Item("Faca", 1);
		legolas.ganharItem(garfo);
		legolas.ganharItem(faca);
		legolas.ganharItem(chave);
		legolas.ganharItem(flechaDeVidro);
		legolas.ganharItem(arcoDeVidro);
		legolas.perderItem(flechaDeVidro);
		legolas.perderItem(garfo);
		assertNotSame(garfo, legolas.getItem(0));
		assertNotSame(faca, legolas.getItem(1));
		assertEquals(arcoDeVidro, legolas.getItem(2));
		assertEquals("Flecha de Vidro", legolas.getItem(3).getNome());

	}

	@Test
	public void testElfoNoturnoAtirarFlachaPerderExperiencia() {
		Elfo legolas = new ElfoNoturno("Legolas");
		Dwarf inimigo = new Dwarf("Inimigo");
		legolas.atirarFlecha(inimigo);
		legolas.atirarFlecha(inimigo);
		assertEquals(6, legolas.getExperiencia());
		assertEquals(70.0, legolas.getVida(), 0.1);

	}

}
