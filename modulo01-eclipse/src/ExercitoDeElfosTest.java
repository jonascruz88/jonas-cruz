import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Test;

public class ExercitoDeElfosTest {
	@After
	public void tearDowm() {
		System.gc();
	}

	@Test
	public void testCriarExercito() {
		ElfoVerde feanor = new ElfoVerde("Feanor");
		ElfoDaLuz will = new ElfoDaLuz("Feanor");
		ExercitoDeElfos batalhao = new ExercitoDeElfos();
		batalhao.alistarElfo(feanor);
		batalhao.alistarElfo(will);
		assertEquals(feanor, batalhao.getElfo(feanor));
		assertEquals(null, batalhao.getElfo(will));
		//assertEquals(2, Elfo.qntInstanciados);
	}

	@Test
	public void testBuscarLista() {
		ElfoVerde feanor = new ElfoVerde("Feanor");
		ElfoNoturno legolas = new ElfoNoturno("Legolas");
		ElfoNoturno verdinho = new ElfoNoturno("Verdinho");
		ElfoVerde joaozinho = new ElfoVerde("Joeozinho");
		ElfoVerde pedrinho = new ElfoVerde("Pedrinho");
		ElfoVerde miguelito = new ElfoVerde("Miguelito");
		ElfoNoturno tony = new ElfoNoturno("Tony Stark");
		ElfoVerde capitao = new ElfoVerde("Capiteo");
		ElfoNoturno viuva = new ElfoNoturno("Vieva Negra");
		ElfoVerde arqueiro = new ElfoVerde("Arqueiro");
		ElfoVerde will = new ElfoVerde("Will Smith");
		ExercitoDeElfos batalhao = new ExercitoDeElfos();
		batalhao.alistarElfo(feanor);
		batalhao.alistarElfo(legolas);
		batalhao.alistarElfo(verdinho);
		batalhao.alistarElfo(joaozinho);
		batalhao.alistarElfo(pedrinho);
		batalhao.alistarElfo(miguelito);
		batalhao.alistarElfo(tony);
		batalhao.alistarElfo(capitao);
		batalhao.alistarElfo(viuva);
		batalhao.alistarElfo(will);
		batalhao.alistarElfo(arqueiro);
		batalhao.getElfo(tony).setStatus(Status.MORTO);
		batalhao.getElfo(will).setStatus(Status.MORTO);
		batalhao.getElfo(arqueiro).setStatus(Status.MORTO);
		batalhao.getElfo(joaozinho).setStatus(Status.MORTO);
		ArrayList<Elfo> mortos = batalhao.buscarLista(Status.MORTO);
		ArrayList<Elfo> vivos = batalhao.buscarLista(Status.VIVO);
		assertEquals(mortos, batalhao.buscarLista(Status.MORTO));
		assertEquals(vivos, batalhao.buscarLista(Status.VIVO));
		assertNotSame(vivos, batalhao.buscarLista(Status.MORTO));
	}

	@Test
	public void testQntAlistamentos() {
		ElfoVerde feanor = new ElfoVerde("Feanor");
		ElfoNoturno legolas = new ElfoNoturno("Legolas");
		ElfoNoturno verdinho = new ElfoNoturno("Verdinho");
		ElfoVerde joaozinho = new ElfoVerde("Joeozinho");
		ElfoVerde pedrinho = new ElfoVerde("Pedrinho");
		ExercitoDeElfos batalhao = new ExercitoDeElfos();
		batalhao.alistarElfo(feanor);
		batalhao.alistarElfo(legolas);
		batalhao.alistarElfo(verdinho);
		batalhao.alistarElfo(joaozinho);
		batalhao.alistarElfo(pedrinho);
		assertEquals(5, batalhao.getQntAlist());
	}

	@Test
	public void testEstrategiaNoturnosPorUltimo() {
		ElfoVerde feanor = new ElfoVerde("Feanor");
		ElfoNoturno legolas = new ElfoNoturno("Legolas");
		ElfoNoturno verdinho = new ElfoNoturno("Verdinho");
		ElfoVerde joaozinho = new ElfoVerde("Joeozinho");
		ElfoVerde pedrinho = new ElfoVerde("Pedrinho");
		ExercitoDeElfos batalhao = new ExercitoDeElfos();
		batalhao.alistarElfo(feanor);
		batalhao.alistarElfo(legolas);
		batalhao.alistarElfo(verdinho);
		batalhao.alistarElfo(joaozinho);
		batalhao.alistarElfo(pedrinho);
		ArrayList<Elfo> esperado = new ArrayList<>();
		esperado.add(feanor);
		esperado.add(joaozinho);
		esperado.add(pedrinho);
		esperado.add(legolas);
		esperado.add(verdinho);
		ArrayList<Elfo> novo = batalhao.NoturnosPorUltimo(batalhao.getBatalhao());
		assertEquals(esperado, novo);
	}

	@Test
	public void testEstrategiaIntercalado() {
		ElfoVerde feanor = new ElfoVerde("Feanor");
		ElfoNoturno legolas = new ElfoNoturno("Legolas");
		ElfoNoturno verdinho = new ElfoNoturno("Verdinho");
		ElfoVerde joaozinho = new ElfoVerde("Joeozinho");
		ElfoVerde pedrinho = new ElfoVerde("Pedrinho");
		ExercitoDeElfos batalhao = new ExercitoDeElfos();
		batalhao.alistarElfo(feanor);
		batalhao.alistarElfo(legolas);
		batalhao.alistarElfo(verdinho);
		batalhao.alistarElfo(joaozinho);
		batalhao.alistarElfo(pedrinho);
		ArrayList<Elfo> esperado = new ArrayList<>();
		esperado.add(feanor);
		esperado.add(verdinho);
		esperado.add(joaozinho);
		esperado.add(legolas);
		// considerando que o ataque � 50% de cada, numero de elemntos impar, o ultimo nao entra. 
		ArrayList<Elfo> novo = batalhao.AtaqueIntercalado(batalhao.getBatalhao());
		assertEquals(esperado, novo);
	}

}