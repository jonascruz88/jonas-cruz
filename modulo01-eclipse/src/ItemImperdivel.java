

public class ItemImperdivel extends Item {
    public ItemImperdivel(String descricao, int quantidade) {
        super(descricao, quantidade);
    }

    @Override
    public void setQnt(int novaQuantidade) {
        boolean podeAlterar = novaQuantidade > 0;
        this.qnt = podeAlterar ? novaQuantidade : 1;
    }
}