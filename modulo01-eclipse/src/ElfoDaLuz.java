import java.util.ArrayList;
import java.util.Arrays;

public class ElfoDaLuz extends Elfo {

	private int qntAtaques;
	private static final double QTD_GANHAR_VIDA = 10;
	private static final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(Arrays.asList("Espada de Galvorn"));

	public ElfoDaLuz(String nome) {
		super(nome);
		this.ganharItem(new ItemImperdivel("Espada De Galvorn", 1));
		QTD_DANO = 21;
	}

	public void atacarComGalvorn(Dwarf dwarf) {
		dwarf.perderVida();
		qntAtaques++;
		boolean devePerderVida = qntAtaques % 2 != 0;
		if (devePerderVida) {
			this.perderVida();
			return;
		}
		this.ganharVida();
	}

	@Override

	public void perderItem(Item item) {
		boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getNome());
		if (possoPerder) {
			super.perderItem(item);
		}

	}

	private void ganharVida() {
		vida += this.QTD_GANHAR_VIDA;
	}

}
