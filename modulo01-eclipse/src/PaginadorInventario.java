import java.util.ArrayList; 

public class PaginadorInventario { 

    private Inventario inventario; 
    private int marcador; 

    public PaginadorInventario(Inventario inventario){ 
        this.inventario = inventario; 
    } 

    public void pular(int marcador){ 
        this.marcador = marcador; 
    } 

    public ArrayList<Item> limitar(int exibe){ 
        if(marcador + exibe <= inventario.inventarioSize()){
            ArrayList <Item> pagina = new ArrayList<>(); 
            for(int i = 0  ; i <= exibe -1; i++) 
                pagina.add(inventario.obter(marcador++)); 
            return pagina; 
        }
        return null;
    } 
} 
