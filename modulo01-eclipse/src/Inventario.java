import java.util.ArrayList; 
public class Inventario { 

    private ArrayList<Item> mochila; 

    public Inventario() { 
        mochila = new ArrayList<>(); 
    } 

    public void adicionar(Item item) { 
        if (item != null) { 
            mochila.add(item); 
        } 
    } 

    public Item obter(int indice) { 
        return mochila.get(indice); 
    } 

    public void remover(int y) { 
        if (y >= 0) 
            mochila.remove(y); 
    } 

    public String getNomesItens() { 
        String s = ""; 
        int last = 0; 
        for (int i = 0; i < mochila.size(); i++) { 
            s = s + mochila.get(i).getNome(); 
            last++; 
            if (mochila.size() > last) 
                s = s + ","; 
        } 
        return s; 
    } 

    public Item maiorQnt() { 
        int maior = 0; 
        Item retorno = null; 
        for (int i = 0; i < mochila.size(); i++) 
            if (mochila.get(i).getQnt() > maior) { 
                retorno = mochila.get(i); 
            } 
        return retorno; 

    } 

    public Item buscar(String nomeItem) { 
        for (int i = 0; i < mochila.size(); i++) 
            if (mochila.get(i).getNome().equals(nomeItem)) 
                return mochila.get(i); 
        return null; 
    } 

    public ArrayList<Item> inverter() { 
        int last = mochila.size() - 1; 
        ArrayList<Item> retorno = new ArrayList<>(); 
        for (int i = 0; i < mochila.size(); i++) { 
            retorno.add(mochila.get(last--)); 
        } 
        return retorno; 

    } 

    public int inventarioSize() { 
        return mochila.size(); 
    } 

    public void ordenarItens() { 
        int menor = 0; 
        for (int i = 0; i < mochila.size(); i++) { 
            for (int j = i + 1; j < mochila.size(); j++) 
                if (mochila.get(j).getQnt() < mochila.get(i).getQnt()) { 
                    Item item = mochila.remove(j); 
                    mochila.add(menor, item); 
                } 
            menor++; 
        } 
    } 

    public void ordenar(TipoOrdenacao tipoOrden){ 
        if(tipoOrden == TipoOrdenacao.ASC){ 
            ordenarItens(); 
            return; 
        } 
        int maior = 0; 
        for (int i = 0; i < mochila.size(); i++) { 
            for (int j = i + 1; j < mochila.size(); j++) 
                if (mochila.get(j).getQnt() > mochila.get(i).getQnt()) { 
                    Item item = mochila.remove(j); 
                    mochila.add(maior, item); 
                } 
            maior++; 
        } 
    } 
} 
