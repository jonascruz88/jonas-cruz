public class Numero{
    
    private int numero;
    public Numero(int numero){
        this.numero = numero;
    }
    
    
    public boolean impar(){
        if(numero%2!=0)
        return true;
        return false;
        //resolvendo com ternario
        //return (numero%2!=0) ? true : false ; 
        // ou apenas
        //return (numero%2!=0) pq o ternario retorna booleano
    }
}