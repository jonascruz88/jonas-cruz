public class EstatisticasInventario { 

    private Inventario inventario; 

    public EstatisticasInventario(Inventario inventario){ 
        this.inventario = inventario; 
    } 

    public double calcularMedia(){ 
        if(inventario == null)
            return Double.NaN;
        double soma = 0;	 
        for (int i = 0; i < inventario.inventarioSize(); i++) { 
            soma += inventario.obter(i).getQnt(); 
        } 
        return soma/inventario.inventarioSize(); 
    } 

    public double calcularMediana(){ 
        if (this.inventario.inventarioSize() == 0) {
            return Double.NaN;
        }
        int qtdItens = this.inventario.inventarioSize();
        int meio = qtdItens / 2;
        boolean qtdImpar = qtdItens % 2 == 1;
        if (qtdImpar) {
            return this.inventario.obter(meio).getQnt();
        }
        int qtdMeioMenosUm = this.inventario.obter(meio - 1).getQnt();
        int qtdMeio = this.inventario.obter(meio).getQnt();
        return (qtdMeioMenosUm + qtdMeio) / 2.; 
    } 

    public int qtdItensAcimaDaMedia(){ 
        int media = (int) calcularMedia(); 
        int qntAcima = 0; 
        for (int i = 0; i < inventario.inventarioSize(); i++) { 
            if(inventario.obter(i).getQnt() > media) 
                qntAcima++; 
        } 
        return qntAcima; 
    } 

}