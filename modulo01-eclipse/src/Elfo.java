
public class Elfo extends Personagem {

    protected int experiencia;
    protected int QTD_EXPERIENCIA;
    protected static int qntInstanciados;

    public Elfo(String nomeInformado) {
        super(nomeInformado, 100.0, Status.VIVO);
        super.ganharItem(new Item("Flecha", 7));
        super.ganharItem(new Item("Arco", 1));
        experiencia = 0;
        QTD_EXPERIENCIA = 1;
        qntInstanciados++;
        QTD_DANO = 0;
    }
    
    protected void finalize() throws Throwable{ 
        qntInstanciados--;
    }

    public String getNome() {
        return nome;
    }

    public void atirarFlecha(Dwarf dwarf) {
        Item flecha = getFlecha();
        if (flecha.getQnt() > 0) {
            flecha.setQnt(flecha.getQnt() - 1);
            experiencia += this.QTD_EXPERIENCIA;
            dwarf.perderVida();
            this.perderVida();
        }

    }

    public Item getFlecha() {
        return this.inventario.buscar("Flecha");
    }

    public int getExperiencia() {
        return experiencia;
    }

    public int getQntInstanciados(){
        return qntInstanciados;
    }

    public int hashCode() {
        return this.nome.hashCode();
    }
}