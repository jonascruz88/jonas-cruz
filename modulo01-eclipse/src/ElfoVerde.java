
public class ElfoVerde extends Elfo {

	public ElfoVerde(String nome) {
		super(nome);
		QTD_EXPERIENCIA *= 2;
	}


	public void ganharItem(Item item) {
		String nomeAuxiliar = item.getNome();
		if (nomeAuxiliar.equals("Espada de Aco Valiriano") || nomeAuxiliar.equals("Arco de Vidro")
				|| nomeAuxiliar.equals("Flecha de Vidro"))
			inventario.adicionar(item);
	}

}
