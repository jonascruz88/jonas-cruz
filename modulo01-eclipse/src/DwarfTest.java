
import static org.junit.Assert.*;

import org.junit.Test;

public class DwarfTest
{
    @Test
    public void dwarfPerde10DeVida(){
        Dwarf dwarf = new Dwarf("Bernardin");
        dwarf.perderVida();
        assertEquals(100, dwarf.getVida(), 0.1);
    }

    @Test
    public void dwarfNasceComStatusVivo(){
        Dwarf dwarf = new Dwarf("Carlos filho de Odin");
        
        assertEquals(Status.VIVO, dwarf.getStatus());
    }
    @Test
    public void dwarfComZeroVidasStatusMorto(){
    	Dwarf lula = new Dwarf("Lula");
        Elfo bolsonaro = new Elfo("Bolsonaro");
        Elfo daciolo = new Elfo("Daciolo");
        bolsonaro.atirarFlecha(lula);
        bolsonaro.atirarFlecha(lula);
        bolsonaro.atirarFlecha(lula);
        bolsonaro.atirarFlecha(lula);
        bolsonaro.atirarFlecha(lula);
        bolsonaro.atirarFlecha(lula);
        bolsonaro.atirarFlecha(lula);
        daciolo.atirarFlecha(lula);
        daciolo.atirarFlecha(lula);
        daciolo.atirarFlecha(lula);
        daciolo.atirarFlecha(lula);
        daciolo.atirarFlecha(lula);
        daciolo.atirarFlecha(lula);
        daciolo.atirarFlecha(lula);
        assertEquals(0, lula.getVida(), 0.1);
        assertEquals(Status.MORTO, lula.getStatus());
    }
}
