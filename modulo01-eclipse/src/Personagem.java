
public abstract class Personagem {

    protected String nome;
    protected double vida;
    protected Status status;
    protected Inventario inventario;
    protected double QTD_DANO;

    public Personagem(String nome, double vida, Status status) {
        this.nome = nome;
        this.vida = vida;
        this.status = status;
        inventario = new Inventario();
    }

    public String getNome() {
        return this.nome;
    }

    public double getVida() {
        return this.vida;
    }

    public Status getStatus() {
        return status;
    }

    protected void setStatus(Status status) {
        this.status = status;
    }

    public void ganharItem(Item item) {
        inventario.adicionar(item);
    }

    public void perderItem(Item item) {
        for (int i = 0; i < inventario.inventarioSize(); i++)
            if (item.equals(inventario.obter(i)))
                inventario.remover(i);
    }

    public Inventario getInventario() {
        return inventario;
    }

    public Item getItem(int n) {
        return inventario.obter(n);
    }

    public void perderVida() {
        vida -= vida >= QTD_DANO ? QTD_DANO : vida;
        if (vida == 0.0) {
            this.status = Status.MORTO;
        }
    }

}
