
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

public class ElfoTest {
    @After
    public void tearDowm(){
        System.gc();
    }

    @Test
    public void criarElfoInformandoNome() {
        Elfo legolas = new Elfo("Legolas");
        assertEquals("Legolas", legolas.getNome());
    }

    @Test
    public void atirarFlechas() {
        Elfo legolas = new Elfo("Legolas");
        Dwarf inimigo = new Dwarf("Inimigo");
        legolas.atirarFlecha(inimigo);
        assertEquals(6, legolas.getFlecha().getQnt());
        assertEquals(1, legolas.getExperiencia());
        assertEquals(1, Elfo.qntInstanciados);
    }
}