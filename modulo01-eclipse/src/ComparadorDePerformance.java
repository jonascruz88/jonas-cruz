
import java.util.*;

public class ComparadorDePerformance {
    public void comparar() {
        ArrayList<Elfo> arrayElfos = new ArrayList<Elfo>();
        HashMap<String, Elfo> mapaElfos = new HashMap<>();
        int qtdElfos = 1000000;
        
        for (int i = 0; i < qtdElfos; i++) {
            String nome = "Elfo " + i;
            Elfo elfo = new Elfo(nome);
            arrayElfos.add(elfo);
            mapaElfos.put(nome, elfo);
        }

        long m1Inicio = System.currentTimeMillis();
        Elfo elfoArrayList = pesquisarArrayList(arrayElfos, "Elfo 1000000");
        long m1Fim = System.currentTimeMillis();

        long m2Inicio = System.currentTimeMillis();
        Elfo elfoMapa = pesquisarMapa(mapaElfos, "Elfo 1000000");
        long m2Fim = System.currentTimeMillis();

        String tempo1 = String.format("%.10f", (m1Fim - m1Inicio) / 1000.0);
        String tempo2 = String.format("%.10f", (m2Fim - m2Inicio) / 1000.0);
        System.out.println("ArrayList:" + tempo1);
        System.out.println("HashMap:" + tempo2);
    }

    private Elfo pesquisarArrayList(ArrayList<Elfo> listaElfos, String nome) {
        for (Elfo elfo : listaElfos) {
            if (elfo.getNome().equals(nome)) {
                return elfo;
            }
        }
        return null;
    }

    private Elfo pesquisarMapa(HashMap<String, Elfo> mapaElfos, String nome) {
        return mapaElfos.get(nome);
    }
}
