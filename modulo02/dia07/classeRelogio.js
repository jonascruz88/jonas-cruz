function relogio(){
    const btnParar = document.getElementById('btnPararRelogio')
    const btnReiniciar = document.getElementById('btnReiniciarRelogio')
    class Relogio{
        constructor(){
        this.horario = document.getElementById('horario'),
        btnReiniciar.disabled = true,
        this.horario.innerText = new Date().toLocaleTimeString()
        this.idInterval = setInterval(function (){
            horario.innerText = new Date().toLocaleTimeString()}, 100)
        }
        
        pararRelogio(){
            btnReiniciar.disabled = false
            clearInterval(this.idInterval)
        }
        reiniciarRelogio(){
            this.horario.innerText = new Date().toLocaleTimeString()
            this.idInterval = setInterval(function (){
                horario.innerText = new Date().toLocaleTimeString()}, 100) 
            btnReiniciar.disabled = true
        }
    }
    const relogio = new Relogio()

    btnParar.onclick = function () {
    relogio.pararRelogio()
    }

    btnReiniciar.onclick = function () {
    relogio.reiniciarRelogio()
    }
    
}   
relogio()