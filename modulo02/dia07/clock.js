function rodarPrograma(){

    const h1 = document.getElementById('horario')
    h1.innerText = new Date()
    
    offBtn()

    const idInterval = setInterval( function() {
        h1.innerText = new Date().toLocaleTimeString()
    }, 100)

    
    const btnPararRelogio = document.getElementById('btnPararRelogio')
    btnPararRelogio.onclick = function () {
        clearInterval(idInterval);
        document.getElementById('btnReiniciarRelogio').disabled = false
        
    }
    const btnReiniciarRelogio = document.getElementById('btnReiniciarRelogio')
        btnReiniciarRelogio.onclick = function() {
        rodarPrograma()
    }
    function offBtn (){
        document.getElementById('btnReiniciarRelogio').disabled = true
    }
}
rodarPrograma()
