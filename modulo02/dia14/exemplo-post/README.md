## Instalar projeto

1. Abra um terminal e navegue até a pasta `exemplo-post`
    * npm install -g json-server
    * npm install

## Rodar projeto

1. Abra um terminal e navegue até a pasta `exemplo-post`
    * json-server --watch db/dados.json
2. Abra outro terminal e navegue até a pasta `exemplo-post`
    * npm run serve
