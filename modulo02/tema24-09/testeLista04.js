describe( 'soma pares', function() {
    beforeEach( function() {
      chai.should()
    })
    it ( 'passar parametro vazio deve retornar 0', function() {
      const array = ""
      const resultado = somarPosicaoPares(array)
      resultado.should.equal(0)
    })
    it ( 'exemplo definido no exercicio retorna 3.34', function() {
      const array =  [ 1, 56, 4.34, 6, -2 ] 
      const resultado = somarPosicaoPares(array)
      resultado.should.equal(3.34)
    })
    it ( 'retorna 755', function() {
        const array =  [ 805, 47, -100, 87, 50 ] 
        const resultado = somarPosicaoPares(array)
        resultado.should.equal(755)
    })
})
describe( 'mostra Elfos', function() {
    beforeEach( function() {
      chai.should()
    })
    it ( 'exemplo passado no exercicio', function() {
        const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 6 }
        const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
        const arrayElfos =([ elfo1, elfo2 ])
        const resultado = formatarElfos(arrayElfos)
        const esperado = ([{nome: "LEGOLAS", temExperiencia: false, qtdFlechas: 6, descricao: "LEGOLAS sem experiência e 6 flechas"
        },{nome: "GALADRIEL", temExperiencia: true, qtdFlechas: 1, descricao: "GALADRIEL com experiência e 1 flecha"}])
        chai.expect(resultado).to.eql(esperado)
    })
    it ( 'exemplo passando 4 elfos', function() {
      const elfo1 = { nome: "Ragnar", experiencia: 2, qtdFlechas: 2 }
      const elfo2 = { nome: "Kjartan", experiencia: 5, qtdFlechas: 0 }
      const elfo3 = { nome: "Golias", experiencia: 4, qtdFlechas: 1 }
      const elfo4 = { nome: "Gandalf", experiencia: 0, qtdFlechas: 4 }
      const arrayElfos =([ elfo1, elfo2, elfo3, elfo4 ])
      const resultado = formatarElfos(arrayElfos)
      const esperado = ([{nome: "RAGNAR", temExperiencia: true, qtdFlechas: 2, descricao: "RAGNAR com experiência e 2 flechas"},
      {nome: "KJARTAN", temExperiencia: true, qtdFlechas: 0, descricao: "KJARTAN com experiência e 0 flechas"},
      {nome: "GOLIAS", temExperiencia: true, qtdFlechas: 1, descricao: "GOLIAS com experiência e 1 flecha"},
      {nome: "GANDALF", temExperiencia: false, qtdFlechas: 4, descricao: "GANDALF sem experiência e 4 flechas"}])
      chai.expect(resultado).to.eql(esperado)
  })
})