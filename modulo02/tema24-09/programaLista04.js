
function somarPosicaoPares(numeros){
    var soma = 0
    for(var i = 0; i < numeros.length ; i++){
        if(i %  2 === 0){
            soma +=  numeros[i]
        }
    }
    return soma
}
function formatarElfos(arrayElfos){
    var index = 0
    for (var elfo of arrayElfos){
        var temExp = false, 
        statusExp = "sem", 
        stringNome = elfo.nome.toUpperCase(),
        statusFlechas = elfo.qtdFlechas + " flecha"
        if(elfo.qtdFlechas!==1){
            statusFlechas+="s"
        }
        if(elfo.experiencia>0){
            temExp = true,
            statusExp = "com"
        }
        var descricaoGeral =  `${stringNome} ${statusExp} experiência e ${statusFlechas}`
        // a formatação da variavel descricaoGeral acima, bota tudo entre aspas duplas
        var elfoAux = {nome: stringNome, temExperiencia: temExp, qtdFlechas: elfo.qtdFlechas, descricao: descricaoGeral }
        arrayElfos[index++] = elfoAux
        delete elfo.experiencia
    }
    return arrayElfos
}
