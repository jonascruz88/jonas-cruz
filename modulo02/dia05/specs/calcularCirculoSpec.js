describe( 'calcularCirculo', function() {
  const expect = chai.expect

  // antes de cada teste, chama a biblioteca "chai" para poder usar as asserções de forma mais amigável (should, expect)
  // documentação: https://www.chaijs.com/
  beforeEach( function() {
    chai.should()
  } )

  describe( 'área', function() {
    // cada bloco "it" é um cenário de teste, como se fosse um @Test do JUnit
    // importante: garanta que o arquivo onde o código está implementado foi incluído, em rodar-testes.html
    it( 'deve calcular área de raio 1', function() {
      const raio = 1, tipoCalculo = 'A'
      const resultado = calcularCirculo( { raio, tipoCalculo } )
      resultado.should.equal( Math.PI )
    } )

    it( 'deve calcular área de raio 0', function() {
      const raio = 0, tipoCalculo = 'A'
      const resultado = calcularCirculo( { raio, tipoCalculo } )
      resultado.should.equal( 0 )
    } )

    it( 'deve calcular área de raio -10', function() {
      const raio = -10, tipoCalculo = 'A'
      const resultado = calcularCirculo( { raio, tipoCalculo } )
      resultado.should.equal( 314.1592653589793 )
    } )
  } )
  

  it( 'deve retornar undefined para tipo desconhecido', function() {
    const raio = 0, tipoCalculo = 'ALABAMA'
    const resultado = calcularCirculo( { raio, tipoCalculo } )
    //const tipoResultado = typeof resultado
    //tipoResultado.should.equal( 'undefined' )
    expect( resultado ).to.be.undefined
  } )

  describe( 'circunferência', function() {
    // isso é um cenário pendente, é preciso implementar o código no segundo parâmetro, que é uma function.
    it( 'deve calcular circunferência de raio 1', function() {
      const raio = 1, tipoCalculo = 'C'
      const resultado = calcularCirculo( { raio, tipoCalculo } )
      resultado.should.equal( 6.283185307179586 )
    } )

    // isso é um cenário pendente, é preciso implementar o código no segundo parâmetro, que é uma function.
    it( 'deve calcular circunferência de raio 0', function() {
      const raio = 0, tipoCalculo = 'C'
      const resultado = calcularCirculo( { raio, tipoCalculo } )
      resultado.should.equal( 0 )
    } )

    it( 'deve calcular circunferência de raio -10', function() {
      const raio = -10, tipoCalculo = 'C'
      const resultado = calcularCirculo( { raio, tipoCalculo } )
      resultado.should.equal( -62.83185307179586 )
    } )
  } )
} )
