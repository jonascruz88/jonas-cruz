
function calcularCirculo(tipoCalc){
        if(tipoCalc.tipo === "A"){
            return Math.PI * Math.pow(tipoCalc.raio,2)
        }
        if(tipoCalc.tipo === "C"){
            return Math.PI*(tipoCalc.raio*2)
        }
}
function naoBissexto(ano){
    if((ano % 4 === 0 && ano % 100 !== 0) || ano % 400 ===0){
        return false
    }
    else
        return true 
}
function  concatenarSemUndefined (item1, item2){
    if (item1 === undefined){
        return item2
    }
    else if (item2 === undefined){
        return item1
    }
    else{
        return item1+item2
    }
}
function  concatenarSemNull (item1, item2){
    if (item1 === null){
        return item2
    }
    else if (item2 === null){
        return item1
    }
    else{
        return item1+item2
    }
}
function  concatenarEmPaz (item1, item2){
    if (item1 === null || item1 === undefined){
        return item2
    }
    else if (item2 === null || item2 === undefined){
        return item1
    }
    else{
        return item1+item2
    }
}
// adicionar (3)(4)
function adicionar(numero1){
    // currying
    // closure, fechamento
    return function(numero2){
        return numero1 + numero2
    }
}
function fiboSum(numero){
    var numero = numero
    var sequencia = [{}]
    var soma, contador = 0
    for (var i = 0 ; i < numero; i++){
        if(i===0){
            sequencia[i] = 1
        }
        else if(i===1){
            sequencia[i] = 1
        }
        else{
            sequencia[i] = sequencia[i-1] + sequencia[i-2]
        }
    }
    function calcFibo(){
        if(contador===numero){
            return soma
        }
        
        soma = soma + sequencia[contador]
        return soma + calcFibo(contador + 1)   
    }  
}
  


