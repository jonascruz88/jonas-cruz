describe( 'calcularCirculo', function() {

    // antes de cada teste, chama a biblioteca "chai" para poder usar as asserções de forma mais amigável (should, expect)
    // documentação: https://www.chaijs.com/
    beforeEach( function() {
      chai.should()
    } )
  
    // cada bloco "it" é um cenário de teste, como se fosse um @Test do JUnit
    // importante: garanta que o arquivo onde o código está implementado foi incluído, em rodar-testes.html
    it( 'deve calcular área de raio 3', function() {
      const raio = 3, tipo = "A"
      const resultado = calcularCirculo( {raio,tipo} )
      resultado.should.equal( Math.PI*Math.pow(raio,2))
    } )
    it( 'deve calcular circunferência de raio 7', function() {
      const raio = 7 , tipo = "C"
      const resultado = calcularCirculo({raio, tipo})
      resultado.should.equal(Math.PI*(raio*2))
    })
  })
describe( 'concatenaSemUndefined', function() {
  beforeEach( function() {
    chai.should()
  })
  it ( 'deve concatenar duas variaveis', function() {
    const firstWord = "Uthred", secondWord = ", son of Uthred"
    const resultado = concatenarSemUndefined(firstWord,secondWord)
    resultado.should.equal( "Uthred, son of Uthred")
  })
  it ( 'deve retornar somente a primeira variavel recebendo "undefined" como segunda', function() {
    const firstWord = "Uthred Ragnarson"
    const resultado = concatenarSemUndefined(firstWord,undefined)
    resultado.should.equal( "Uthred Ragnarson")
  })
  it ( 'deve retornar somente uma variavel sem receber a outra', function() {
    const firstWord = "Ragnarson, O Jovem"
    const resultado = concatenarSemUndefined(firstWord)
    resultado.should.equal( "Ragnarson, O Jovem")
  })
  it ( 'deve retornar somente a segunda variavel recebendo "undefined" como primeira', function() {
    const secondWord = "Conde Guthrum"
    const resultado = concatenarSemUndefined(undefined, secondWord)
    resultado.should.equal( "Conde Guthrum")
  })
})
describe( 'concatenaSemNull', function() {
  beforeEach( function() {
    chai.should()
  })
  it ( 'deve concatenar duas variaveis', function() {
    const firstWord = "George", secondWord = "Orwell"
    const resultado = concatenarSemNull(firstWord,secondWord)
    resultado.should.equal( "GeorgeOrwell")
  })
  it ( 'deve retornar somente a primeira variavel recebendo "null" como segunda', function() {
    const firstWord = 1984
    const resultado = concatenarSemNull(firstWord,null)
    resultado.should.equal(1984)
  })
  it ( 'deve retornar somente a variavel informada + "undefined" sem receber a outra', function() {
    const firstWord = "Revolução dos bichos"
    const resultado = concatenarSemNull(firstWord)
    resultado.should.equal( "Revolução dos bichosundefined")
  })
  it ( 'deve retornar somente a segunda variavel recebendo "null" como primeira', function() {
    const secondWord = "teletela"
    const resultado = concatenarSemNull(null, secondWord)
    resultado.should.equal( "teletela")  
  })
})
describe( 'concatenaEmPaz', function() {
  beforeEach( function() {
    chai.should()
  })
  it ( 'deve concatenar duas variaveis', function() {
    const firstWord = "Porto Alegre, ", secondWord = 16+"/"+8+"/"+2006
    const resultado = concatenarEmPaz(firstWord,secondWord)
    resultado.should.equal( "Porto Alegre, 16/8/2006")
  })
  it ( 'deve retornar somente a primeira variavel recebendo "null" como segunda', function() {
    const firstWord = "Palmeiras não tem mundial"
    const resultado = concatenarEmPaz(firstWord,null)
    resultado.should.equal("Palmeiras não tem mundial")
  })
  it ( 'deve retornar somente a segunda variavel recebendo "null" como primeira', function() {
    const secondWord = "P. Vittar melhor vocal"
    const resultado = concatenarEmPaz(null,secondWord)
    resultado.should.equal("P. Vittar melhor vocal")
  })
  it ( 'deve retornar somente a primeira variavel recebendo "undefined" como segunda', function() {
    const firstWord = 123456789
    const resultado = concatenarEmPaz(firstWord,undefined)
    resultado.should.equal(123456789)
  })
  it ( 'deve retornar somente a segunda variavel recebendo "undefined" como primeira', function() {
    const secondWord = true
    const resultado = concatenarEmPaz(undefined,secondWord)
    resultado.should.equal(true)
  })
  it ( 'deve retornar somente uma variavel sem receber a outra', function() {
    const firstWord = 0.20
    const resultado = concatenarSemUndefined(firstWord)
    resultado.should.equal(0.20)
  })
})

describe( 'naoBissexto', function() {
  beforeEach( function() {
    chai.should()
  })
  it ( '2016 é bissexto', function() {
    const year = 2016
    const resultado = naoBissexto(year)
    resultado.should.equal(false)
  })
  it ( '2017 não é bissexto', function() {
    const year = 2017
    const resultado = naoBissexto(year)
    resultado.should.equal(true)
  })
  it ( '2018 não é bissexto', function() {
    const year = 2018
    const resultado = naoBissexto(year)
    resultado.should.equal(true)
  })
  it ( '2019 não é bissexto', function() {
    const year = 2019
    const resultado = naoBissexto(year)
    resultado.should.equal(true)
  })
  it ( '2020 é bissexto', function() {
    const year = 2020
    const resultado = naoBissexto(year)
    resultado.should.equal(false)
  })
})
describe( 'soma dois paramentros', function() {
  beforeEach( function() {
    chai.should()
  })
  it ( '(3)(4)', function() {
    const soma = 3+4
    const resultado = adicionar(3)(4)
    resultado.should.equal(soma)
  })
  it ( '(5642)(8749)', function() {
    const soma = 5642+8749
    const resultado = adicionar(5642)(8749)
    resultado.should.equal(soma)
  })
})