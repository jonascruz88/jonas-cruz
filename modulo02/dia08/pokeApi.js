class pokeApi{
    constructor(url){
        this.url = url
    }

    buscarElemento(numeroID){
        if(numeroID > 0 && numeroID < 803){
            let urlConcat = `${ this.url }/${ numeroID }/`
            return fetch(urlConcat)
            }
        else{
            alert('Digite um id válido')
        }
    }
}