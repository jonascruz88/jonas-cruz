class Pokemon{
    constructor(dadosJsonVindoApi){
            this.nome = dadosJsonVindoApi.name
            this.imagem = dadosJsonVindoApi.sprites.front_default
            this.id = dadosJsonVindoApi.id
            this.altura = dadosJsonVindoApi.height
            this.peso = dadosJsonVindoApi.weight
            for (const tipo of dadosJsonVindoApi.types) {
                const p = document.createElement('p')
                p.innerText = tipo.type.name
                dadosPokemon.appendChild(p)
            }
            for (const tipo of dadosJsonVindoApi.stats){                       
                    const p = document.createElement('p') 
                    p.innerText = tipo.stat.name
                    dadosPokemon.appendChild(p)
            }
        }
}