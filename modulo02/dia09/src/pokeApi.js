class pokeApi{
    constructor(url){
        this.url = url
    }
    async buscarElemento(numeroID){
        if(numeroID > 0 && numeroID < 803){
            let urlConcat = `${ this.url }/${ numeroID }/`
            return new Promise( resolve => {
                    fetch( urlConcat )
                    .then( j => j.json() )
                    .then( p => {
                      const pokemon = new Pokemon( p )
                    resolve( pokemon )
                    } )
                } )
              }
        else{
            alert('Digite um ID válido!')
        }
    }
}