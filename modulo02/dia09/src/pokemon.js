class Pokemon{
    constructor(dadosJsonVindoApi){
            this.nome = dadosJsonVindoApi.name
            this.imagem = dadosJsonVindoApi.sprites.front_default
            this.id = dadosJsonVindoApi.id
            this.altura = dadosJsonVindoApi.height
            this.peso = dadosJsonVindoApi.weight
            this.tipo = dadosJsonVindoApi.types.map( tipo => tipo.type.name)
            this.estatistica = dadosJsonVindoApi.stats.map( estatistica => estatistica.stat.name)
            /*for (const tipo of dadosJsonVindoApi.types) {
                const p = document.createElement('li')
                p.innerText = tipo.type.name
                meuPrimeiroApp.appendChild(p)
            }
            for (const tipo of dadosJsonVindoApi.stats){                       
                const p = document.createElement('li') 
                p.innerText = tipo.stat.name
                meuPrimeiroApp.appendChild(p)
            }*/
        }
}