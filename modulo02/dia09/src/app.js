const urlApi = 'https://pokeapi.co/api/v2/pokemon'
const pokeapi = new pokeApi( urlApi )

let app = new Vue({
    el: '#meuPrimeiroApp',
    data: {
        idParaBuscar : '',
        pokemon: {}
    },
    methods: {
        async buscarElemento() {
            this.pokemon = await pokeapi.buscarElemento(this.idParaBuscar)
        }
    }
})