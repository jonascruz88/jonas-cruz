function rodarPrograma(){

    const dadosPokemon = document.getElementById('meuPrimeiroApp')
    const h1 = document.getElementById('h1')
    const img = document.getElementById('#thumb')
    let idParaBuscar = document.getElementById('idParaBuscar')
    let altura = document.getElementById('altura')
    let peso = document.getElementById('peso')
    let numeroDigitado = document.getElementById('numeroPokemon') 
    //let estouComSorte = document.getElementById('estouComSorte')

    const url = 'https://pokeapi.co/api/v2/pokemon'
    const pokeapi = new pokeApi(url)

    estouComSorte.onclick = function(){
        var valor = Math.floor(Math.random()*801+1)
        pokeapi.buscarElemento(valor)
        .then( res => res.json() )
        .then( dadosJson => { 
            const pokemon = new Pokemon( dadosJson )
            renderizaNaTela( pokemon )
            })    
    }
    idParaBuscar.onblur = function(){
        var valorDoCampo = numeroDigitado.value
        pokeapi.buscarElemento(valorDoCampo)
        .then( res => res.json() )
        .then( dadosJson => { 
            const pokemon = new Pokemon( dadosJson )
            renderizaNaTela( pokemon )
            } )
    }
    function renderizaNaTela( pokemon ){
        h1.innerText = pokemon.nome
        img.src = pokemon.imagem
        id.innerText = pokemon.id
        altura.innerText = pokemon.altura
        peso.innerText = pokemon.peso
    }
}
rodarPrograma()