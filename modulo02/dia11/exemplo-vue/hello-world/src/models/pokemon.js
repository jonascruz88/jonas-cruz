export default class Pokemon{
    constructor(dadosJsonVindoApi){
            this.nome = dadosJsonVindoApi.name
            this.imagem = dadosJsonVindoApi.sprites.front_default
            this.id = dadosJsonVindoApi.id
            this.altura = dadosJsonVindoApi.height*10
            this.peso = dadosJsonVindoApi.weight/10
            this.tipo = dadosJsonVindoApi.types.map( tipo => tipo.type.name)
            this.estatistica = dadosJsonVindoApi.stats.map( estatistica => estatistica.stat.name)
        }
}