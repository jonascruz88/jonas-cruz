import Pokemon from '../models/pokemon.js'

export default class PokeApi {
  constructor( url ) {
    this.url = url
  }

  async listarPorTipo( idTipo, numPage, limitPage ) {
    const urlTipo = `https://pokeapi.co/api/v2/type/${ idTipo }/`
    let startPage = 0
    let pokemons
    return new Promise(resolve => 
      fetch( urlTipo )
        .then( j => j.json() )
        .then( t => {
          if(numPage>1){
              limitPage = limitPage*numPage,
              startPage = limitPage/2
              pokemons = t.pokemon.slice( startPage , limitPage)
          }
          else{
              pokemons = t.pokemon.slice( startPage, limitPage )
          }
          const promisesPkm = pokemons.map( p => this.buscarPorUrl( p.pokemon.url ) )
          Promise.all( promisesPkm ).then( resultadoFinal => {              
            resolve(resultadoFinal)
          })
        }))
  }
  
  async buscarPorUrl( urlPokemon ) {
    return new Promise( resolve => {
      fetch( urlPokemon )
        .then( j => j.json() )
        .then( p => {
          const pokemon = new Pokemon( p )
          resolve( pokemon )
        } )
    } )
  }

  async buscar( idPokemon ) {
    let urlPokemon = `${ this.url }/${ idPokemon }/`
    return this.buscarPorUrl( urlPokemon )
  }
  
}
