function formatarElfos( elfos ) {
  elfos.forEach( elfo => {
    elfo.nome = elfo.nome.toUpperCase()
    elfo.temExperiencia = elfo.experiencia > 0
    delete elfo.experiencia
    
    const textoExperiencia = elfo.temExperiencia ? 'com' : 'sem'
    const textoFlechas = elfo.qtdFlechas !== 1 ? 's' : ''
    const comFlechas = elfo.qtdFlechas !== 1 ? 'e com' : 'e'

    elfo.descricao = `${ elfo.nome } ${ textoExperiencia } experiência ${ comFlechas } ${ elfo.qtdFlechas } flecha${ textoFlechas }`
  } )
  return elfos
}
