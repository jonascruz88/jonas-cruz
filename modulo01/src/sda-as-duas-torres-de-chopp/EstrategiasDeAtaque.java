import java.awt.List;
import java.util.ArrayList;

public interface EstrategiasDeAtaque {
	
	ArrayList <Elfo> NoturnosPorUltimo(ArrayList<Elfo> atacantes);
	ArrayList <Elfo> AtaqueIntercalado(ArrayList<Elfo> atacantes);

}
