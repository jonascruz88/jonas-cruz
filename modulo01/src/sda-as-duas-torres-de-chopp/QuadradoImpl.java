
public class QuadradoImpl implements FormaGeometrica {
    private int x, y;

    public void setX(int x) {
        this.x = x;
        this.y = x;
    }

    public void setY(int y) {
        this.y = y;
        this.x = y;
    }

    public int calcularArea() {
        return this.x * this.y;
    }
}