import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;
public class DadoD6Test{
    
    
    @Test
    public void DadoD6ComDadoFakeTest(){       
        DadoFake dado = new DadoFake();
        dado.simularValor(5);
        int esperado = dado.sortear();
        assertTrue(esperado<=6);
    }

}
