import static org.junit.Assert.*; 
 
import org.junit.Test; 
 
public class EstatisticasInventarioTest { 
	 
	@Test 
	public void testCacularMedia() { 
		Inventario invent = new Inventario(); 
		EstatisticasInventario testEstat = new EstatisticasInventario(invent); 
		Item casaco = new Item("Casaco", 2); 
		Item chave = new Item("Chave", 8); 
		Item espada = new Item("Espada", 4); 
		Item cuia = new Item("Cuia", 3); 
		Item termica = new Item("Termica", 5); 
		Item celular = new Item("Celular", 6); 
		invent.adicionar(casaco); 
		invent.adicionar(chave); 
		invent.adicionar(espada); 
		invent.adicionar(cuia); 
		invent.adicionar(termica); 
		invent.adicionar(celular); 
		assertEquals(4.66, testEstat.calcularMedia(), 0.1); 
	} 
	@Test 
	public void testCacularMediana() { 
		Inventario invent = new Inventario(); 
		EstatisticasInventario testEstat = new EstatisticasInventario(invent); 
		Item casaco = new Item("Casaco", 2); 
		Item chave = new Item("Chave", 8); 
		Item espada = new Item("Espada", 4); 
		Item cuia = new Item("Cuia", 7); 
		Item termica = new Item("Termica", 5); 
		Item celular = new Item("Celular", 6); 
		invent.adicionar(casaco); 
		invent.adicionar(chave); 
		invent.adicionar(espada); 
		invent.adicionar(cuia); 
		invent.adicionar(termica); 
		invent.adicionar(celular); 
		assertEquals(5.5, testEstat.calcularMediana(), 0.1); 
	} 
	@Test 
	public void testQntAcimaMedia() { 
		Inventario invent = new Inventario(); 
		EstatisticasInventario testEstat = new EstatisticasInventario(invent); 
		Item casaco = new Item("Casaco", 2); 
		Item chave = new Item("Chave", 8); 
		Item espada = new Item("Espada", 4); 
		Item cuia = new Item("Cuia", 7); 
		Item termica = new Item("Termica", 5); 
		Item celular = new Item("Celular", 6); 
		invent.adicionar(casaco); 
		invent.adicionar(chave); 
		invent.adicionar(espada); 
		invent.adicionar(cuia); 
		invent.adicionar(termica); 
		invent.adicionar(celular); 
		assertEquals(3, testEstat.qtdItensAcimaDaMedia()); 
		assertNotSame(5, testEstat.qtdItensAcimaDaMedia()); 
		 
	} 
 
} 
