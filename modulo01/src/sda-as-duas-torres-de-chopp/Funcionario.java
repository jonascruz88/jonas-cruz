public class Funcionario{
    
    private String nome;
    private double salario;
    private double totVendas;
    
    public Funcionario(String nome, double salario, double totVendas){
        this.nome = nome;
        this.salario = salario;;
        this.totVendas = totVendas;
    }
    
    public double calculaLucro(){
        double lucro = 0;
        double comissao = 0;
        salario = salario - (salario*0.10);
        comissao = (totVendas * 0.15);
        comissao = comissao - (comissao*0.10);
        lucro = salario + comissao;
        return lucro;
    }
    }
    