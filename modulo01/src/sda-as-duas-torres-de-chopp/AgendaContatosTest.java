import static org.junit.Assert.*;
import org.junit.Test;

public class AgendaContatosTest {

	@Test
	public void testAgendaAdicionarBuscarPorNome() {
		AgendaContatos agenda = new AgendaContatos();
		agenda.adicionar("Bernardo", "555555");
		agenda.adicionar("Mithrandir", "444444");
		agenda.adicionar("Maria", "777777");
		assertEquals("555555", agenda.consultarTelefonePorNome("Bernardo"));
	}

	@Test
	public void testAgendaAdicionarBuscarPorTelefone() {
		AgendaContatos agenda = new AgendaContatos();
		agenda.adicionar("Bernardo", "555555");
		agenda.adicionar("Mithrandir", "444444");
		String esperado = agenda.consultarPorTelefone("444444");
		assertEquals(esperado, agenda.consultarPorTelefone("444444"));
	}

	@Test
    public void testAgendaAdicionarGerarCSV(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        agenda.adicionar("Mithrandir", "444444");
        agenda.adicionar("Maria", "777777");
        String separador = System.lineSeparator();
        String aux = String.format("Mithrandir,444444%sBernardo,555555%sMaria,777777%s", separador, separador, separador);
        assertEquals(aux, agenda.csv());
	}
}