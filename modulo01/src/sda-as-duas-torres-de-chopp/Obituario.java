
import java.util.*;

public class Obituario {
    
    private static int qtdConsultas = 0;
    
    private HashMap<Elfo, ArrayList<Dwarf>> dwarvesPorElfo = new HashMap<>();

    public static int getQtdConsultas() {
        return Obituario.qtdConsultas;
    }
    
    public ArrayList<Dwarf> mortosPor(Elfo elfo) {
        Obituario.qtdConsultas++;
        return dwarvesPorElfo.get(elfo);
    }

    public void registrarMorte(Elfo elfo, Dwarf dwarf) {
        ArrayList<Dwarf> listaAtualDoElfo = this.mortosPor(elfo);
        boolean primeiroDwarfVinculado = listaAtualDoElfo == null;
        if (primeiroDwarfVinculado) {
            listaAtualDoElfo = new ArrayList<>();
            dwarvesPorElfo.put(elfo, listaAtualDoElfo);
        }
        listaAtualDoElfo.add(dwarf);
    }
}