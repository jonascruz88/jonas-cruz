
import java.util.HashMap;

public class AgendaContatos {

    private HashMap<String, String> agenda = new HashMap<>();

    protected void adicionar(String nome, String telefone) {
        agenda.put(nome, telefone);
    }

    public String consultarTelefonePorNome(String nome) {
        String contato = agenda.get(nome);
        return contato;
    }

    public String consultarPorTelefone(String telefone){
        String auxiliar = null;
        for (String chave : agenda.keySet()) {
            String s = agenda.get(chave);
            if(s.equals(telefone))
                auxiliar = agenda.get(chave);
        }
        return auxiliar;
    }

    public String csv() {
        StringBuilder builder = new StringBuilder();

        for(HashMap.Entry<String, String> par : agenda.entrySet()){
            String separador = System.lineSeparator();
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s,%s%s", chave, valor, separador);
            builder.append(contato);
            //builder.append(chave + "," + valor + "\n");
            //String s, retorno = "";
            //int i = 0;
            //for (String chave : agenda.keySet()) {
            //	i++;
            //	s = agenda.get(chave);
            //	retorno = retorno + chave + "," + s;
            //	if (i < agenda.keySet().size())
            //		retorno = retorno + "\n";
            //}
            //return retorno;
        }
        return builder.toString();
    }
}