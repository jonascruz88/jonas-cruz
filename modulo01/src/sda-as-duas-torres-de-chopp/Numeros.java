public class Numeros { 
 
	private double[] entrada; 
 
	public Numeros(double[] entrada) { 
		this.entrada = entrada; 
	} 
 
	public double[] calcularMediaSeguinte() { 
		double[] aux = new double[entrada.length - 1]; 
		for (int i = 0; i < entrada.length - 1; i++) { 
			aux[i] = (entrada[i] + entrada[i + 1]) / 2; 
		} 
		return aux; 
	} 
 
} 
