public class Item extends Object{

    private String nome;
    protected int qnt;

    public Item(String nome, int qnt){
        this.nome = nome;
        this.setQnt(qnt);
    }

    public String getNome(){
        return nome;
    }

    public int getQnt(){
        return qnt;
    }

    public void setQnt(int i){
        qnt = i;
    }

    public boolean equals(Object outroItem){
        Item outro = (Item) outroItem;
        boolean iguais = this.nome.equals(outro.getNome())
            && this.getQnt() == outro.getQnt();
        return iguais;
    }
}