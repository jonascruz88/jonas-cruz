import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ExercitoDeElfos implements EstrategiasDeAtaque {

	private ArrayList<Elfo> exercito = new ArrayList<>();
	private static final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
			Arrays.asList(ElfoVerde.class, ElfoNoturno.class));
	private HashMap<Status, ArrayList<Elfo>> listaPorStatus = new HashMap<>();
	private int qntAlistamentos;

	protected void alistarElfo(Elfo elfo) {
		if (TIPOS_PERMITIDOS.contains(elfo.getClass())) {
			exercito.add(elfo);
			ArrayList<Elfo> elfosPorStatus = listaPorStatus.get(elfo.getStatus());
			if (elfosPorStatus == null) {
				elfosPorStatus = new ArrayList<>();
				listaPorStatus.put(elfo.getStatus(), elfosPorStatus);
			}
			elfosPorStatus.add(elfo);
			qntAlistamentos++;
		}
	}

	public ArrayList<Elfo> getBatalhao() {
		return this.exercito;
	}

	public Elfo getElfo(Elfo elfo) {
		return exercito.contains(elfo) ? elfo : null;
	}

	public ArrayList<Elfo> buscarLista(Status status) {
		return this.listaPorStatus.get(status);
	}

	public int getQntAlist() {
		return qntAlistamentos;
	}

	public ArrayList<Elfo> NoturnosPorUltimo(ArrayList<Elfo> atacantes) {
		ArrayList<Elfo> retorno = new ArrayList<>();
		atacantes = this.buscarLista(Status.VIVO);
		for (int i = 0; i < atacantes.size(); i++) {
			if (atacantes.get(i).getClass().equals(ElfoVerde.class))
				retorno.add(atacantes.get(i));
		}
		for (int i = 0; i < atacantes.size(); i++) {
			if (atacantes.get(i).getClass().equals(ElfoNoturno.class))
				retorno.add(atacantes.get(i));
		}
		return retorno;
	}

	public ArrayList<Elfo> AtaqueIntercalado(ArrayList<Elfo> atacantes) {
		ArrayList<Elfo> lista = this.NoturnosPorUltimo(atacantes);
		ArrayList<Elfo> retorno = new ArrayList<>();
		int y = lista.size() - 1;
		for (int i = 0; i < y ; i++) {
			retorno.add(lista.get(i));
			retorno.add(lista.get(y));
			y--;
		}
		return retorno;

	}

}
