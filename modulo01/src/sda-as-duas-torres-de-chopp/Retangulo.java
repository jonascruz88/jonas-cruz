public class Retangulo{

    private int x, y;

    public int getY(){
        return y;
    }

    public int getX(){
        return x;
    }

    public void setY(int y){
        this.y = y;
    }

    public void setX(int x){
        this.x = x;
    }

    public int calcularArea(){
        return x*y;
    }
}
