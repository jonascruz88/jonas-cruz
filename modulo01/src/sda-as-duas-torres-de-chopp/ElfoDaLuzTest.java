import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import org.junit.After;
import org.junit.Test;

public class ElfoDaLuzTest {

    @After
    public void tearDowm(){
        System.gc();
    }

    @Test
    public void testElfoDaLuzAtacaDwarf() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gemlim = new Dwarf("Gemlim");
        feanor.atacarComGalvorn(gemlim);
        assertEquals(79, feanor.getVida(), 0.1);
    }

    @Test
    public void testElfoDaLuzAtacaDwarfDuasVezes() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComGalvorn(gul);
        feanor.atacarComGalvorn(gul);
        assertEquals(89, feanor.getVida(), 0.1);

    }

    @Test
    public void testElfoDaLuzAtacaDwarfCincoVezes() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf farlum = new Dwarf("Farlum");
        feanor.atacarComGalvorn(farlum);
        feanor.atacarComGalvorn(farlum);
        feanor.atacarComGalvorn(farlum);
        feanor.atacarComGalvorn(farlum);
        feanor.atacarComGalvorn(farlum);
        assertEquals(57, feanor.getVida(), 0.1);
    }

}
