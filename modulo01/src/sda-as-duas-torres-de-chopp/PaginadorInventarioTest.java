import static org.junit.Assert.*; 
 
import java.util.ArrayList; 
 
import org.junit.Test; 
 
public class PaginadorInventarioTest { 
	 
	@Test 
	public void testPaginador() { 
		Inventario invent = new Inventario(); 
		PaginadorInventario testPaginador = new PaginadorInventario(invent); 
		Item casaco = new Item("Casaco", 2); 
		Item chave = new Item("Chave", 8); 
		Item espada = new Item("Espada", 4); 
		Item cuia = new Item("Cuia", 7); 
		Item termica = new Item("Termica", 5); 
		Item celular = new Item("Celular", 6); 
		invent.adicionar(casaco); 
		invent.adicionar(chave); 
		invent.adicionar(espada); 
		invent.adicionar(cuia); 
		invent.adicionar(termica); 
		invent.adicionar(celular); 
		testPaginador.pular(3); 
		ArrayList <Item> testArray = testPaginador.limitar(3);		 
		assertEquals(testArray.get(0), invent.obter(3)); 
		assertEquals(testArray.get(1), invent.obter(4)); 
		assertEquals(testArray.get(2), invent.obter(5));		 
		assertNotSame(testArray.get(1), invent.obter(1)); 
		 
	} 
 
 
} 
