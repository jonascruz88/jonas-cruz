import static org.junit.Assert.*; 
import org.junit.Test; 
import java.util.*; 

public class InventarioTest { 
    @Test 
    public void testAdd() { 
        Inventario invent = new Inventario(); 
        Item casaco = new Item("Casaco", 2); 
        invent.adicionar(casaco); 
        assertEquals(casaco, invent.obter(0)); 
    } 

    @Test 
    public void testAdd2x() { 
        Inventario invent = new Inventario(); 
        Item casaco = new Item("Casaco", 2); 
        Item chave = new Item("Chave", 3); 
        invent.adicionar(casaco); 
        invent.adicionar(chave); 
        assertEquals(casaco, invent.obter(0)); 
        assertEquals(chave, invent.obter(1)); 
    } 

    @Test(expected = IndexOutOfBoundsException.class) 
    public void testRemover() { 
        Inventario invent = new Inventario(); 
        Item casaco = new Item("Casaco", 2); 
        Item chave = new Item("Chave", 3); 
        invent.adicionar(casaco); 
        invent.adicionar(chave); 
        invent.remover(1); 
        assertEquals(casaco, invent.obter(0)); 
        assertEquals(1, invent.obter(1)); 
    } 

    @Test 
    public void testObterNomes() { 
        Inventario invent = new Inventario(); 
        Item casaco = new Item("Casaco", 2); 
        Item chave = new Item("Chave", 3); 
        invent.adicionar(casaco); 
        invent.adicionar(chave); 
        assertEquals("Casaco,Chave", invent.getNomesItens()); 

    } 

    @Test 
    public void testObterNomesRemovendoDoMeio() { 
        Inventario invent = new Inventario(); 
        Item casaco = new Item("Casaco", 2); 
        Item chave = new Item("Chave", 3); 
        Item espada = new Item("Espada", 2); 
        Item cuia = new Item("Cuia", 3); 
        Item termica = new Item("Termica", 2); 
        Item celular = new Item("Celular", 3); 
        invent.adicionar(casaco); 
        invent.adicionar(chave); 
        invent.adicionar(espada); 
        invent.adicionar(cuia); 
        invent.adicionar(termica); 
        invent.adicionar(celular); 
        invent.remover(0); 
        invent.remover(3); 
        assertEquals("Chave,Espada,Cuia,Celular", invent.getNomesItens()); 

    } 

    @Test 
    public void testBuscarItem() { 
        Inventario invent = new Inventario(); 
        Item casaco = new Item("Casaco", 2); 
        Item chave = new Item("Chave", 3); 
        Item espada = new Item("Espada", 2); 
        Item cuia = new Item("Cuia", 3); 
        invent.adicionar(casaco); 
        invent.adicionar(chave); 
        invent.adicionar(espada); 
        invent.adicionar(cuia); 
        assertEquals(chave, invent.buscar("Chave")); 
        assertNotSame(cuia, invent.buscar("Casaco")); 
    } 

    @Test 
    public void testInverterArrayList(){ 
        Inventario invent = new Inventario(); 
        invent.adicionar(new Item("Espada", 1)); 
        invent.adicionar(new Item("Escudo", 2)); 
        ArrayList <Item> aux = new ArrayList<>(); 
        aux = invent.inverter(); 
        assertEquals(invent.obter(0), aux.get(1)); 
        assertEquals(invent.obter(1), aux.get(0)); 
    } 

    @Test 
    public void testInverterArrayListMaior(){ 
        Inventario invent = new Inventario(); 
        invent.adicionar(new Item("Espada", 1)); 
        invent.adicionar(new Item("Escudo", 2)); 
        invent.adicionar(new Item("Casaco", 1)); 
        invent.adicionar(new Item("Cuia", 2)); 
        invent.adicionar(new Item("Termica", 1)); 
        invent.adicionar(new Item("Chave", 2)); 
        ArrayList <Item> aux = new ArrayList<>(); 
        aux = invent.inverter(); 
        assertEquals(invent.obter(0), aux.get(5)); 
        assertEquals(invent.obter(1), aux.get(4)); 
        assertEquals(invent.obter(2), aux.get(3)); 
        assertEquals(invent.obter(3), aux.get(2)); 
        assertEquals(invent.obter(4), aux.get(1)); 
        assertEquals(invent.obter(5), aux.get(0)); 
    } 

    @Test 
    public void testInverterArrayListMaiorForcandoErro(){ 
        Inventario invent = new Inventario(); 
        invent.adicionar(new Item("Espada", 1)); 
        invent.adicionar(new Item("Escudo", 2)); 
        invent.adicionar(new Item("Casaco", 1)); 
        invent.adicionar(new Item("Cuia", 2)); 
        invent.adicionar(new Item("Termica", 1)); 
        invent.adicionar(new Item("Chave", 2)); 
        ArrayList <Item> aux = new ArrayList<>(); 
        aux = invent.inverter(); 
        assertNotSame(invent.obter(0), aux.get(0)); 
        assertNotSame(invent.obter(1), aux.get(1)); 
        assertNotSame(invent.obter(2), aux.get(2)); 
        assertNotSame(invent.obter(3), aux.get(3)); 
        assertNotSame(invent.obter(4), aux.get(4)); 
        assertNotSame(invent.obter(5), aux.get(5)); 
    } 

    @Test 
    public void testOrdenaItens() { 
        Inventario invent = new Inventario(); 
        Item casaco = new Item("Casaco", 47); 
        Item chave = new Item("Chave", 55); 
        Item espada = new Item("Espada", 24); 
        Item cuia = new Item("Cuia", 109); 
        invent.adicionar(casaco); 
        invent.adicionar(chave); 
        invent.adicionar(espada); 
        invent.adicionar(cuia); 
        invent.ordenarItens(); 
        assertEquals(chave, invent.obter(2)); 
        assertNotSame(cuia, invent.obter(1)); 
    } 

    @Test 
    public void testOrdenaItensUsandoTipo() { 
        Inventario invent = new Inventario(); 
        Item casaco = new Item("Casaco", 47); 
        Item chave = new Item("Chave", 55); 
        Item espada = new Item("Espada", 24); 
        Item cuia = new Item("Cuia", 109); 
        Item termica = new Item("Termica", 2); 
        Item celular = new Item("Celular", 3); 
        invent.adicionar(casaco); 
        invent.adicionar(chave); 
        invent.adicionar(espada); 
        invent.adicionar(cuia); 
        invent.adicionar(termica); 
        invent.adicionar(celular); 
        invent.ordenar(TipoOrdenacao.ASC); 
        assertEquals(casaco, invent.obter(3)); 
        assertNotSame(cuia, invent.obter(1)); 
        invent.ordenar(TipoOrdenacao.DESC); 
        assertEquals(cuia, invent.obter(0)); 
        assertEquals(termica, invent.obter(5)); 

    } 

}