--CRIAR TABELA CLIENTE COM SEQUENCIA DE ID AUTOMATICA
CREATE TABLE CLIENTE (
  ID INTEGER NOT NULL PRIMARY KEY,
  NOME VARCHAR2(100) NOT NULL
);
CREATE SEQUENCE CLIENTE_SEQ;
--CRIAR TABELA ANIMAL COM SEQUENCIA DE ID AUTOMATICA
CREATE TABLE ANIMAL(
  ID INTEGER NOT NULL PRIMARY KEY,
  NOME VARCHAR2(100) NOT NULL
);
CREATE SEQUENCE ANIMAL_SEQ;


--CRIAR RELACIONAMENTO MANY TO MANY
CREATE TABLE CLIENTE_ANIMAL(
  ID_CLIENTE INTEGER NOT NULL REFERENCES CLIENTE(ID),
  ID_ANIMAL INTEGER NOT NULL REFERENCES ANIMAL(ID)
);

COMMIT;