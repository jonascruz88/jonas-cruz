/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.entity.exercicio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author tiago
 */
@Entity
@Table(name = "SISTEMA")
public class Sistema implements Serializable {

    @Id
    @SequenceGenerator(name = "SISTEMA_SEQ", sequenceName = "SISTEMA_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "SISTEMA_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "NOME", nullable = false, length = 100)
    private String nome;
    @JoinColumn(name = "SISTEMA_PAI", referencedColumnName = "ID")
    @ManyToOne
    private Sistema sistemaPai;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSistema")
    private List<UsuarioSistema> usuarioSistemaList;

    public Sistema() {
    }

    public Sistema(String nome, Sistema sistemaPai) {
        this.nome = nome;
        this.sistemaPai = sistemaPai;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Sistema getSistemaPai() {
        return sistemaPai;
    }

    public void setSistemaPai(Sistema sistemaPai) {
        this.sistemaPai = sistemaPai;
    }

    public List<UsuarioSistema> getUsuarioSistemaList() {
        return usuarioSistemaList;
    }

    public void setUsuarioSistemaList(List<UsuarioSistema> usuarioSistemaList) {
        this.usuarioSistemaList = usuarioSistemaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sistema)) {
            return false;
        }
        Sistema other = (Sistema) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.dbc.petshop.entity.exercicio.Sistema[ id=" + id + " ]";
    }
    
}
