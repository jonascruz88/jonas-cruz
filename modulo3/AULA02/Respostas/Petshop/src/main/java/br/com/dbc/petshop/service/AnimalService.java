/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.SexoType;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;

/**
 *
 * @author tiago
 */
public class AnimalService {

    private static final AnimalService instance;

    static {
        instance = new AnimalService();
    }

    public static AnimalService getInstance() {
        return instance;
    }

    private AnimalService() {
    }
    
    public Animal create(Animal c) {
        EntityManager em = PersistenceUtils.getEm();
        try {
            em.getTransaction().begin();
            c = em.merge(c);
            em.getTransaction().commit();
            return c;
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }
    }

    public void batchCreate(List<Animal> clientes) {
        EntityManager em = PersistenceUtils.getEm();
        try {
            em.getTransaction().begin();
            clientes.forEach(em::persist);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }
    }
    
    public List<Animal> findAll(){
        EntityManager em = PersistenceUtils.getEm();
        List<Animal> animais = em.createQuery("select a from Animal a").getResultList();
        return animais;
    }
    
    public List<Animal> findAllCriteria(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createCriteria(Animal.class).list();
    }

    List<Animal> insere10Animais(Cliente cliente) {
        List<Animal> animais = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            animais.add(new Animal("Animal"+i, SexoType.M, cliente, new Double(i)));
        }

        batchCreate(animais);
        
        cliente.setAnimalList(animais);
        
        return animais;
    }
    
}
