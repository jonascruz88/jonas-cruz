/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.exercicio.Sistema;
import br.com.dbc.petshop.entity.exercicio.Usuario;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author tiago
 */
public class SistemaService {

    private static final SistemaService instance;

    static {
        instance = new SistemaService();
    }

    public static SistemaService getInstance() {
        return instance;
    }

    private SistemaService() {
    }

    public List<Sistema> findAll() {
        EntityManager em = PersistenceUtils.getEm();
        List<Sistema> sistemas = em.createQuery("select a from Sistema a").getResultList();
        return sistemas;
    }

    public List<Sistema> findAllCriteria() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createCriteria(Sistema.class).list();
    }

    public Sistema create(Sistema c) {
        EntityManager em = PersistenceUtils.getEm();
        try {
            em.getTransaction().begin();
            c = em.merge(c);
            em.getTransaction().commit();
            return c;
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }
    }

    public void batchCreate(List<Sistema> sistemas) {
        EntityManager em = PersistenceUtils.getEm();
        try {
            em.getTransaction().begin();
            sistemas.forEach(em::persist);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }
    }

    public Sistema createCriteria(Sistema c) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            session.save(c);
            t.commit();
            return c;
        } catch (Exception e) {
            t.rollback();
            throw e;
        }
    }

    public List<Sistema> insere2SistemasCom5Usuarios() {
        List<Sistema> sistemas = new ArrayList<>();

        Sistema pai = create(new Sistema("Sistema Pai", null));
        Sistema filho = create(new Sistema("Sistema Filho", pai));

        sistemas.add(pai);
        sistemas.add(filho);

        List<Usuario> usuarios = UsuarioService
                .getInstance()
                .insere10Usuarios();

        List<Usuario> primeiros5 = usuarios
                .stream()
                .limit(5)
                .collect(Collectors.toList());
        List<Usuario> ultimos5 = usuarios
                .stream()
                .skip(5)
                .collect(Collectors.toList());

        pai.setUsuarioSistemaList(
                UsuarioSistemaService
                .getInstance()
                .vinculaUsuariosComSistema(pai, primeiros5));

        filho.setUsuarioSistemaList(
                UsuarioSistemaService
                .getInstance()
                .vinculaUsuariosComSistema(filho, ultimos5));

        return sistemas;
    }
    
    public boolean hasAccess(Long idSistema, Long idUsuario){
        EntityManager em = PersistenceUtils.getEm();
        return em.createQuery("select count(us) "
                + "FROM UsuarioSistema us "
                + "LEFT JOIN us.idSistema.sistemaPai.usuarioSistemaList usl "
                + "WHERE us.idSistema.id = :idSistema "
                + "and (us.idUsuario.id = :idUsuario "
                + "     or usl.idUsuario.id = :idUsuario) ", Long.class)
                .setParameter("idUsuario", idUsuario)
                .setParameter("idSistema", idSistema)
                .getSingleResult() > 0;
    }

}
