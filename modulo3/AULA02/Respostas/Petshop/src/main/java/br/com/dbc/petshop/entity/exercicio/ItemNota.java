/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.entity.exercicio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tiago
 */
@Entity
@Table(name = "ITEM_NOTA")
public class ItemNota implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ITEM_NOTA_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "ITEM_NOTA_SEQ", sequenceName = "ITEM_NOTA_SEQ")
    private Long id;
    @Column(name = "DESCRICAO", length = 20)
    private String descricao;
    @Enumerated(EnumType.STRING)
    @Column(name = "UM", nullable = false, length = 2)
    private UnidadeMedidaType um;
    @Column(name = "DATA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data;
    @JoinColumn(name = "ID_NOTA", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Nota idNota;
    @JoinColumn(name = "ID_PRODUTO", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Produto idProduto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public UnidadeMedidaType getUm() {
        return um;
    }

    public void setUm(UnidadeMedidaType um) {
        this.um = um;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Nota getIdNota() {
        return idNota;
    }

    public void setIdNota(Nota idNota) {
        this.idNota = idNota;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemNota)) {
            return false;
        }
        ItemNota other = (ItemNota) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.dbc.petshop.entity.exercicio.ItemNota[ id=" + id + " ]";
    }
    
}
