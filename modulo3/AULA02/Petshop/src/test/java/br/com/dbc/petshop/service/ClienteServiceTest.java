/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.SexoType;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tiago
 */
public class ClienteServiceTest {
    
    public ClienteServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    private void createClienteNome(){
    }

    /**
     * Test of findAll method, of class ClienteService.
     */
    //@Test
    public void testFindAll() {
    }

    /**
     * Test of findAllCriteria method, of class ClienteService.
     */
    //@Test
    public void testFindAllCriteria() {
    }

    /**
     * Test of create method, of class ClienteService.
     */
    //@Test
    public void testCreate() {
    }

    /**
     * Test of createCriteria method, of class ClienteService.
     */
    //@Test
    public void testCreateCriteria() {
    }
    //@Test
    public void testBuscarClientePorNomeCriteria() {
        EntityManager em = PersistenceUtils.getEm();
        //em.getTransaction().begin();
        System.out.println("buscarClientePorNomeCriteria");
        Cliente cliente = new Cliente(null,"Cliente0",SexoType.F,"jjj");
        ClienteService instance = ClienteService.getInstance();
        instance.create(cliente);
        String expNome = "Cliente0";
        List<Cliente> result = instance.buscarClientePorNomeCriteria("Cliente0");
        assertEquals(1, result.size());
        assertEquals(expNome, result.get(0).getNome());
    }

    /**
     * Test of getInstance method, of class ClienteService.
     */
    //@Test
    public void testGetInstance() {
    }

    /**
     * Test of insereCliente10Animais method, of class ClienteService.
     */
    //@Test
    public void testInsereCliente10Animais() {
        EntityManager em = PersistenceUtils.getEm();
        System.out.println("insereCliente10Animais");
        ClienteService instance = ClienteService.getInstance();     
        instance.insereCliente10Animais();
        List<Cliente> result = em.createQuery("select c from Cliente c", Cliente.class).getResultList();
        assertEquals(10, result.size());
        assertEquals("Cliente1", result.get(1).getNome());
        
    }
    
}
