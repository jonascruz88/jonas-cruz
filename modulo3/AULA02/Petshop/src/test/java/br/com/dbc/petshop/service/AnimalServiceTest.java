/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.SexoType;
import java.util.List;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jonas.cruz
 */
public class AnimalServiceTest {
    
    public AnimalServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class AnimalService.
     */
    //@Test
    public void testGetInstance() {
        System.out.println("getInstance");
        AnimalService expResult = null;
        AnimalService result = AnimalService.getInstance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findAll method, of class AnimalService.
     */
    //@Test
    public void testFindAll() {
        System.out.println("findAll");
        AnimalService instance = null;
        List<Animal> expResult = null;
        List<Animal> result = instance.findAll();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findAllCriteria method, of class AnimalService.
     */
    //@Test
    public void testFindAllCriteria() {
        System.out.println("findAllCriteria");
        AnimalService instance = null;
        List<Animal> expResult = null;
        List<Animal> result = instance.findAllCriteria();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of insere10Animais method, of class AnimalService.
     */
    //@Test
    public void testInsere10Animais() {
        System.out.println("insere10Animais");
        Cliente cliente = null;
        AnimalService instance = null;
        instance.insere10Animais(cliente);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of insere5AnimaisComHibernate method, of class AnimalService.
     */
    @Test
    public void testInsere5AnimaisComHibernate() {
        System.out.println("insere5AnimaisComHibernate");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Cliente cliente = new Cliente(null,"Cralos",SexoType.F,"ccaa");
        AnimalService instance = AnimalService.getInstance();
        session.beginTransaction();
        session.save(cliente);
        session.getTransaction().commit();
        instance.insere5AnimaisComHibernate(cliente);
        List<Animal> result = session.createCriteria(Animal.class).list();
        assertEquals(5, result.size());
    }
}
