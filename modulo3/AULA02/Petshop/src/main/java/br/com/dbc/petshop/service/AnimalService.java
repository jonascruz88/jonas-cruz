/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.SexoType;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Hibernate;
import org.hibernate.Session;

/**
 *
 * @author tiago
 */
public class AnimalService {

    private static final AnimalService instance;

    static {
        instance = new AnimalService();
    }

    public static AnimalService getInstance() {
        return instance;
    }

    private AnimalService() {
    }
    
    public List<Animal> findAll(){
        EntityManager em = PersistenceUtils.getEm();
        List<Animal> animais = em.createQuery("select a from Animal a").getResultList();
        return animais;
    }
    
    public List<Animal> findAllCriteria(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createCriteria(Animal.class).list();
    }
    public void insere10Animais(Cliente cliente){
        EntityManager em = PersistenceUtils.getEm();
        PersistenceUtils.beginTransaction();
        for(int j = 0; j <= 9;j++){
                Animal animal = new Animal();                
                animal.setIdCliente(cliente);
                animal.setNome("Animal"+j);
                animal.setSexo(SexoType.M);                
                animal.setValor(BigDecimal.ZERO);
                em.persist(animal);
        }
        em.getTransaction().commit();
    }
    public void insere5AnimaisComHibernate(Cliente cliente){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        for(int j = 0; j <= 4;j++){
                Animal animal = new Animal();                
                animal.setIdCliente(cliente);
                animal.setNome("Animal"+j);
                animal.setSexo(SexoType.M);                
                animal.setValor(BigDecimal.ZERO);
                session.save(animal);
        }
        session.getTransaction().commit();
    }
    
}
