/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshopjpa.dao;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author jonas.cruz
 */
public class PersistenceUtils {
    
    public static EntityManager em;
    
    static{
        em = Persistence
                .createEntityManagerFactory("petshop")
                .createEntityManager();
    }
    
    public static EntityManager getEntityManager(){
        return em;
    }
    
}
