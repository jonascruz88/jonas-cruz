/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshopjpa.service;

import com.mycompany.petshopjpa.dao.ClienteDAO;
import com.mycompany.petshopjpa.entity.Cliente;
import java.util.List;

/**
 *
 * @author jonas.cruz
 */
public class ClienteService extends AbstractCrudService<Cliente, Long, ClienteDAO> {
    
    private final ClienteDAO clienteDAO;
    
    {
        clienteDAO = new ClienteDAO();
    }
    
    public ClienteDAO getDAO(){
        
        return clienteDAO;
    }
    
}
