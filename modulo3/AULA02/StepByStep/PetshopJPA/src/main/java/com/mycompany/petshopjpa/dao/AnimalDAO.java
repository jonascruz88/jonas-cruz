/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshopjpa.dao;

import com.mycompany.petshopjpa.entity.Animal;

/**
 *
 * @author jonas.cruz
 */
public class AnimalDAO extends AbstractDAO{
    
    protected Class getEntityClass() {
        return Animal.class;
    }
}
