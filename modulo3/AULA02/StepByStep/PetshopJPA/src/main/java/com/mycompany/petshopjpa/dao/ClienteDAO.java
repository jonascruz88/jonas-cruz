/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshopjpa.dao;

import com.mycompany.petshopjpa.entity.Cliente;


/**
 *
 * @author jonas.cruz
 */
public class ClienteDAO extends AbstractDAO<Cliente, Long>{

    
    protected Class getEntityClass() {
        return Cliente.class;
    }
    
}
