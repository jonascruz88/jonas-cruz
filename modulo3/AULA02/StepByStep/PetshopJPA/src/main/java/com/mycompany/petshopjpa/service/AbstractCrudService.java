/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshopjpa.service;

import com.mycompany.petshopjpa.dao.AbstractDAO;
import com.mycompany.petshopjpa.entity.AbstractEntity;
import java.util.List;

/**
 *
 * @author jonas.cruz
 */
public abstract class AbstractCrudService<E extends AbstractEntity<ID>, ID, DAO extends AbstractDAO<E, ID>> {
    
    public abstract DAO getDAO();
    
    public List<E> findAll(){
        return getDAO().findAll();
    }
    
    public E findOne(ID id){
        return getDAO().findOne(id);
    }
    
    public void create(E e){
        getDAO().create(e);
    }
    
    public void update(E e){
        getDAO().update(e);
    }
    
    public void delete(ID id){
        getDAO().delete(id);
    }
    
}
