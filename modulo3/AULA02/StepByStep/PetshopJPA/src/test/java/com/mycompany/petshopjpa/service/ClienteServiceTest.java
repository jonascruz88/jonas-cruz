/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshopjpa.service;

import com.mycompany.petshopjpa.dao.ClienteDAO;
import com.mycompany.petshopjpa.dao.PersistenceUtils;
import com.mycompany.petshopjpa.entity.Animal;
import com.mycompany.petshopjpa.entity.Cliente;
import java.util.Arrays;
import static java.util.Arrays.asList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.internal.util.reflection.Whitebox;

/**
 *
 * @author jonas.cruz
 */
public class ClienteServiceTest {
    
    public ClienteServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        em.getTransaction().begin();
        em.createQuery("delete from Cliente").executeUpdate();
        em.getTransaction().commit();
    }
    
    @After
    public void tearDown() {
    }
    
    private final EntityManager em = PersistenceUtils.getEntityManager();

    @Test
    public void testFindAll() {
        System.out.println("findAll");
        em.getTransaction().begin();
        Cliente c = Cliente.builder()
                .nome("João")
                .animalList(Arrays.asList(Animal
                .builder()
                .nome("Pé de Feijão")
                .build()))
                .build();
        em.persist(c);
        em.getTransaction().commit();
        ClienteService instance = new ClienteService();
        List<Cliente> clientes = instance.findAll();
        assertEquals(1, clientes.size());
        assertEquals(c.getId(), clientes.stream().findFirst().get().getId());
        assertEquals(c.getAnimalList().size(), clientes.stream().findFirst().get().getAnimalList().size());
        assertEquals(c.getAnimalList().get(0).getId(), clientes.stream().findFirst().get().getAnimalList().get(0).getId());
    }
    
    @Test
    public void testFindOne(){
        System.out.println("findOne");
        em.getTransaction().begin();
        Cliente c = Cliente.builder()
                .nome("João")
                .animalList(Arrays.asList(Animal
                .builder()
                .nome("Peppa")
                .build()))
                .build();
        Cliente d = Cliente.builder()
                .nome("Miguel")
                .animalList(Arrays.asList(Animal
                .builder()
                .nome("Axl Rose")
                .build()))
                .build();
        em.persist(c);
        em.persist(d);
        em.getTransaction().commit();
        ClienteService instance = new ClienteService();
        List<Cliente> lista = instance.findAll();
        assertEquals(2, lista.size());
        assertEquals(c.getId(), lista.stream().findFirst().get().getId());
        assertEquals(d.getId(), lista.get(1).getId());
        assertEquals(c.getAnimalList().size(), lista.stream().findFirst().get().getAnimalList().size());
        assertEquals(d.getAnimalList().get(0).getNome(), "Axl Rose");
    }
    
    @Test
    public void testFindOneMocked(){
        System.out.println("findOneMocked");
        ClienteService clienteService = new ClienteService();
        ClienteDAO clienteDAO = Mockito.mock(ClienteDAO.class);
        Whitebox.setInternalState(clienteService, "clienteDAO", clienteDAO);
        Cliente cliente = Cliente.builder()
                .nome("Jovem")
                .animalList(Arrays.asList(Animal.builder()
                    .nome("Velho")
                    .build()))
                    .build();
        Mockito.doReturn(cliente).when(clienteDAO).findOne(cliente.getId());
        Cliente returned = clienteService.findOne(cliente.getId());
        assertEquals(cliente.getId(), returned.getId());
        verify(clienteDAO, times(1)).findOne(cliente.getId());
    }
    
    @Test
    public void testDeleteMocked(){
        System.out.println("deleteMocked");
        ClienteService clienteService = new ClienteService();
        ClienteDAO clienteDAO = Mockito.mock(ClienteDAO.class);
        Whitebox.setInternalState(clienteService, "clienteDAO", clienteDAO);
        Cliente cliente = Cliente.builder()
                .nome("Jovem")
                .animalList(Arrays.asList(Animal.builder()
                    .nome("Velho")
                    .build()))
                    .build();
        Mockito.doNothing().when(clienteDAO).delete(cliente.getId());
        clienteService.delete(cliente.getId());
        verify(clienteDAO, times(1)).delete(cliente.getId());
    }

    
    //@Test
    //public void create(E e){
    //    getDAO().create(e);
    //}
    
    //@Test
    //public void update(E e){
    //    getDAO().update(e);
    //}
    

}
