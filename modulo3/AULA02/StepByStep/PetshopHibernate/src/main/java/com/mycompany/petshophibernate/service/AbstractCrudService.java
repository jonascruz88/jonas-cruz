/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshophibernate.service;

import com.mycompany.petshophibernate.dao.AbstractDAO;
import com.mycompany.petshophibernate.entity.AbstractEntity;
import java.util.List;
import javassist.NotFoundException;

/**
 *
 * @author jonas.cruz
 */
public abstract class AbstractCrudService<ENTITY extends AbstractEntity<ID>, ID, DAO extends AbstractDAO<ENTITY, ID>> {
    
    protected abstract DAO getDAO();
    
    
    
    public List<ENTITY> findAll(){
        return getDAO().findAll();
    }
    
    public ENTITY findOne(ID id){
        return getDAO().findById(id);
    }
    
    public void create(ENTITY entity){
        if(entity.getId() != null){
            throw new IllegalArgumentException("Criação não pode ter ID");
        }
        getDAO().createOrUpdate(entity);
    }
    public void update(ENTITY entity){
    if(entity.getId() == null){
        throw new IllegalArgumentException("Atualização de cliente deve ter ID");
    }
    getDAO().createOrUpdate(entity);
    }
    public void delete(ID id) throws NotFoundException{
        if(id == null){
            throw new IllegalArgumentException("ID deve ser informado");
        }
        ENTITY entity = getDAO().findById(id);
        if(entity == null){
            throw new NotFoundException("");
        }
        getDAO().delete(entity);
    }
    
    
    
}
