/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshophibernate.dao;
import com.mycompany.petshophibernate.entity.Cliente;


/**
 *
 * @author jonas.cruz
 */
public class ClienteDAO extends AbstractDAO<Cliente, Long> {
    
    private static final ClienteDAO instance;
    
    static{
        instance = new ClienteDAO();
    }
    
    public static ClienteDAO getInstance(){
        return instance;
    }
    

    @Override
    protected Class<Cliente> getEntityClass() {
        return Cliente.class;
    }

    @Override
    protected String getIdProperty() {
        return "id";
    }
    
}
