/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshophibernate.service;

import com.mycompany.petshophibernate.dao.AnimalDAO;
import com.mycompany.petshophibernate.entity.Animal;

/**
 *
 * @author jonas.cruz
 */
public class AnimalService extends AbstractCrudService<Animal, Long, AnimalDAO>{
    
    private static AnimalService instance;
    
    static{
        instance = new AnimalService();
    }
    
    
    public static AnimalService getInstance(){
        return instance;
    }
    
    public AnimalDAO getDAO(){
        return AnimalDAO.getInstance();
}
    
    
    
    
}
