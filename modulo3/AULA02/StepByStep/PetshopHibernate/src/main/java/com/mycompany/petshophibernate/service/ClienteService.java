/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshophibernate.service;

import com.mycompany.petshophibernate.dao.ClienteDAO;
import com.mycompany.petshophibernate.entity.Cliente;


/**
 *
 * @author jonas.cruz
 */
public class ClienteService extends AbstractCrudService<Cliente, Long, ClienteDAO>{
    
    private static ClienteService instance;
    
    static{
        instance = new ClienteService();
    }
    
    
    public static ClienteService getInstance(){
        return instance;
    }
    
    public ClienteDAO getDAO(){
        return ClienteDAO.getInstance();
}
    
    
    
}
