/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshophibernate.service;

import com.mycompany.petshophibernate.dao.ClienteDAO;
import com.mycompany.petshophibernate.dao.HibernateUtil;
import com.mycompany.petshophibernate.entity.Animal;
import com.mycompany.petshophibernate.entity.Cliente;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javassist.NotFoundException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author jonas.cruz
 */
public class ClienteServiceTest {
    
    public ClienteServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCreate() {
        System.out.println("create");
        Cliente cliente1 = Cliente.builder()
                .nome("Cliente 1")
                .animalList(Arrays.asList( Animal.builder()
                    .nome("Animal 1")
                    .build()))
                .build();
        ClienteService.getInstance().create(cliente1);
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Cliente> clientes = session.createCriteria(Cliente.class).list();
        Assert.assertEquals("Quantidade de clientes errada", 1 , clientes.size());
        Cliente result = clientes.stream().findAny().get();
        Assert.assertEquals("Cliente diferente", cliente1.getId(), result.getId());
        Assert.assertEquals("Quantidade animais diferentes", cliente1.getAnimalList().size(),
                result.getAnimalList().size());
        Assert.assertEquals("Animais diferentes", cliente1.getAnimalList().stream().findAny().get().getId(),  
                result.getAnimalList().stream().findAny().get().getId());
        session.close();

        
    }
    @Test
    public void testCreateMocked() {
        System.out.println("create mocked");
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        Cliente cliente1 = Cliente.builder()
                .nome("Cliente 1")
                .animalList(Arrays.asList( Animal.builder()
                    .nome("Animal 1")
                    .build()))
                .build();
        clienteService.create(cliente1);
        verify(daoMock, times(1)).createOrUpdate(any());     
    }
    
    @Test
    public void testUpDateMocked() {
        System.out.println("update mocked");
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        Cliente cliente1 = Cliente.builder()
                .id(1L)
                .nome("Cliente 1")
                .animalList(Arrays.asList( Animal.builder()
                    .nome("Animal 1")
                    .build()))
                .build();
        clienteService.update(cliente1);
        verify(daoMock, times(1)).createOrUpdate(any());     
    }
    
    @Test
    public void testDeleteMocked() throws NotFoundException {
        System.out.println("delete mocked");
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        Cliente cliente1 = Cliente.builder()
                .id(1L)
                .nome("Cliente 1")
                .animalList(Arrays.asList( Animal.builder()
                    .nome("Animal 1")
                    .build()))
                .build();
        Mockito.when(clienteService.findOne(cliente1.getId())).thenReturn(cliente1);
        clienteService.delete(cliente1.getId());
        verify(daoMock, times(1)).delete(any());     
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCreateException() {
        System.out.println("create IllegalArgumentException");
        ClienteService.getInstance()
                .create(Cliente.builder().id(1L).build());
    }
    
    @Test(expected = HibernateException.class)
    public void testCreateExceptionMocked() {
        System.out.println("create hibernateException mocked");
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doThrow(new HibernateException("")).when(daoMock).createOrUpdate(any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        Cliente cliente1 = Cliente.builder()
                .nome("Cliente 1")
                .animalList(Arrays.asList( Animal.builder()
                    .nome("Animal 1")
                    .build()))
                .build();
        clienteService.create(cliente1);
    //    verify(daoMock, times(1)).createOrUpdate(any());     
    }
    @Test
    public void testFindAllMocked() {
        System.out.println("findAll mocked");
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        List<Cliente> clientes = new LinkedList<>();
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        Mockito.when(clienteService.findAll()).thenReturn(clientes);
        Cliente cliente1 = Cliente.builder()
                .nome("Cliente 1")
                .animalList(Arrays.asList( Animal.builder()
                    .nome("Animal 1")
                    .build()))
                .build();
        Cliente cliente2 = Cliente.builder()
                .nome("Cliente 2")
                .animalList(Arrays.asList( Animal.builder()
                    .nome("Animal 2")
                    .build()))
                .build();
        clientes.add(cliente1);
        clientes.add(cliente2);
        clienteService.findAll();
        verify(daoMock, times(1)).findAll(); 
    }
    
    @Test
    public void testFindOneMocked() {
        System.out.println("findAll mocked");
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        Cliente cliente1 = Cliente.builder()
                .nome("Cliente 1")
                .animalList(Arrays.asList( Animal.builder()
                    .nome("Animal 1")
                    .build()))
                .build();
        Mockito.when(clienteService.findOne(cliente1.getId())).thenReturn(cliente1);
        clienteService.findOne(cliente1.getId());
        verify(daoMock, times(1)).findById(cliente1.getId()); 
    }
    
}
