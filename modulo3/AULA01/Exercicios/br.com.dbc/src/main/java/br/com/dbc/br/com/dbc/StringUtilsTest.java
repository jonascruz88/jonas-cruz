package br.com.dbc.br.com.dbc;

import org.apache.commons.lang.StringUtils;

public class StringUtilsTest {

	public static void main(String[] args) {
		System.out.println(StringUtils.isBlank(""));
		System.out.println(StringUtils.isBlank(null));
		System.out.println(StringUtils.isBlank(" "));
		System.out.println(StringUtils.isBlank("ee") == false);
		System.out.println(StringUtils.isEmpty(""));
		System.out.println(StringUtils.isEmpty(null));
		System.out.println(StringUtils.isEmpty("gg") == false);
		System.out.println(StringUtils.rightPad("ggg", 10)+"distancia");
		System.out.println(StringUtils.leftPad("ggg", 10)+"distancia");
		System.out.println(StringUtils.trimToNull("       aux       "));
		System.out.println(StringUtils.contains("aux    ", "aux"));

	}

}
