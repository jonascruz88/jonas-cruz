/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhaseguradora.rest;

import br.com.dbc.minhaseguradora.config.SoapConnector;
import br.com.dbc.minhaseguradora.dto.ApoliceDTO;
import br.com.dbc.minhaseguradora.entity.Apolice;
import br.com.dbc.minhaseguradora.service.ApoliceService;
import br.com.dbc.minhaseguradora.ws.ConsultaCEP;
import br.com.dbc.minhaseguradora.ws.ObjectFactory;
import java.util.Objects;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author tiago
 */
@RestController
@RequestMapping("/api/apolice")
public class ApoliceRestController {

    @Autowired
    private ApoliceService apoliceService;

    @GetMapping()
    public ResponseEntity<?> list(Pageable pageable) {
        return ResponseEntity.ok(apoliceService.findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable Long id) {
        ApoliceDTO dto = new ApoliceDTO();
        
        Apolice a = apoliceService.findById(id).get();
        
        dto.setNome(a.getDescricao());
        dto.setPeriodo(a.getInicioVigencia().toString() + "-" + a.getFinalVigencia().toString());
        return ResponseEntity.ok(dto)
                
                ;
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody Apolice input) {
        if (id == null || !Objects.equals(input.getId(), id)) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(apoliceService.save(input));
    }

    @PostMapping
    public ResponseEntity<?> post(@RequestBody Apolice input) {
        if (input.getId() != null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(apoliceService.save(input));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        apoliceService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Error message")
    public void handleError() {
        
    }
    
    @Autowired SoapConnector soapConnector;
    @Autowired ObjectFactory objectFactory;
    
    @GetMapping("/cep/{cep}")
    public ResponseEntity<?> cep(@PathVariable("cep") String cep) {
        ConsultaCEP consulta = objectFactory.createConsultaCEP();
        consulta.setCep(cep);
        return ResponseEntity.ok(soapConnector
                .callWebService(
                "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente", 
                        objectFactory.createConsultaCEP(consulta)));
    }

}
