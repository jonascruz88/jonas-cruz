/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhaseguradora.rest;

import br.com.dbc.minhaseguradora.MinhaSeguradoraApplicationTests;
import br.com.dbc.minhaseguradora.entity.Apolice;
import br.com.dbc.minhaseguradora.repository.ApoliceRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tiago
 */
public class ApoliceRestControllerTest extends MinhaSeguradoraApplicationTests{
    
    public ApoliceRestControllerTest() {
    }
    
    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
    
    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
    
    @Autowired
    private ApoliceRepository apoliceRepository;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @Autowired
    private ApoliceRestController apoliceRestController;
    
    private MockMvc restMockMvc;
    
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.restMockMvc = MockMvcBuilders.standaloneSetup(apoliceRestController)
                .setCustomArgumentResolvers(pageableArgumentResolver)
                .setMessageConverters(jacksonMessageConverter).build();
    }
    
    @Test
    @Transactional
    public void postTest() throws Exception{
        int databaseSizeBeforeCreate = Long.valueOf(apoliceRepository.count()).intValue();

        Apolice apolice = new Apolice();
        apolice.setInicioVigencia(LocalDate.now());
        apolice.setFinalVigencia(LocalDate.now());
        apolice.setDescricao("teste");
        
        restMockMvc.perform(post("/api/apolice")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(apolice)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.descricao").value("teste"))
                .andExpect(jsonPath("$.inicioVigencia").value(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .andExpect(jsonPath("$.finalVigencia").value(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));

        List<Apolice> apolices = apoliceRepository.findAll();
        assertThat(apolices).hasSize(databaseSizeBeforeCreate + 1);
        Apolice testApolice = apolices.stream().findFirst().orElse(null);
        Assert.assertNotNull(testApolice);
        assertThat(testApolice.getDescricao()).isEqualTo("teste");
        assertThat(testApolice.getInicioVigencia()).isEqualTo(LocalDate.now());
        assertThat(testApolice.getFinalVigencia()).isEqualTo(LocalDate.now());
    }
    
}
