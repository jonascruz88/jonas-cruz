/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhafloricultura.rest;

import br.com.dbc.minhafloricultura.dao.ProdutoDAO;
import br.com.dbc.minhafloricultura.entity.Produto;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

/**
 *
 * @author tiago
 */
@Stateless
@Path("produto")
public class ProdutoFacadeREST extends AbstractCrudREST<Produto, ProdutoDAO> {

    @Inject
    private ProdutoDAO produtoDAO;
    
    @Override
    protected ProdutoDAO getDAO() {
        return produtoDAO;
    }
    
}
