/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.minhafloricultura.ws;


import com.mycompany.minhafloricultura.dao.ProdutoDAO;
import com.mycompany.minhafloricultura.entity.Produto;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author jonas.cruz
 */
@Stateless
@WebService(serviceName = "ProdutoWS")
public class ProdutoWS extends AbstractCrudWS<ProdutoDAO, Produto>{

    @EJB(beanInterface = ProdutoDAO.class)
    private ProdutoDAO produtoDAO;

    @Override
    @WebMethod(exclude = true)
    public ProdutoDAO getDAO() {
        return produtoDAO;
    }
    

    
    
}
