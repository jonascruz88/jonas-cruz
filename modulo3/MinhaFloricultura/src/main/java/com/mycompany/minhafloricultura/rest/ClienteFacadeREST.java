/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.minhafloricultura.rest;

import com.mycompany.minhafloricultura.dao.ClienteDAO;
import com.mycompany.minhafloricultura.entity.Cliente;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

/**
 *
 * @author jonas.cruz
 */
@Stateless
@Path("com.mycompany.minhafloricultura.entity.cliente")

public class ClienteFacadeREST extends AbstractCrudRest<Cliente, ClienteDAO> {
    
    @Inject
    private ClienteDAO clienteDAO;
    
    @Override
    protected ClienteDAO getDAO() {
        return clienteDAO;
    }
    
}
