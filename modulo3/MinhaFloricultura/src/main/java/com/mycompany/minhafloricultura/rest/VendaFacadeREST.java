/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.minhafloricultura.rest;

import com.mycompany.minhafloricultura.dao.VendaDAO;
import com.mycompany.minhafloricultura.entity.Venda;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

/**
 *
 * @author jonas.cruz
 */
@Stateless
@Path("venda")
public class VendaFacadeREST extends AbstractCrudRest<Venda, VendaDAO> {

    @Inject
    private VendaDAO vendaDAO;
    
    @Override
    protected VendaDAO getDAO() {
        return vendaDAO;
    }
    
}
