/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.minhafloricultura.rest;

import com.mycompany.minhafloricultura.dao.ProdutoDAO;
import com.mycompany.minhafloricultura.entity.Produto;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

/**
 *
 * @author jonas.cruz
 */
@Stateless
@Path("produto")
public class ProdutoFacadeREST extends AbstractCrudRest<Produto, ProdutoDAO> {

    @Inject
    private ProdutoDAO produtoDAO;
    
    @Override
    protected ProdutoDAO getDAO() {
        return produtoDAO;
    }
    
}
