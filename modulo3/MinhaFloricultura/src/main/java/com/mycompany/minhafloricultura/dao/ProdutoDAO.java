/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.minhafloricultura.dao;

import com.mycompany.minhafloricultura.entity.Produto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jonas.cruz
 */
@Stateless
public class ProdutoDAO extends AbstractDAO<Produto> {

    @PersistenceContext(unitName = "minha_floricultura_pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProdutoDAO() {
        super(Produto.class);
    }
    
    //public Produto findByName(String descricao){
    //    Produto produto = c;
    //    return getEntityManager()
    //}
    
}
