/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhaseguradora.service;

import br.com.dbc.minhaseguradora.entity.Bem;
import br.com.dbc.minhaseguradora.repository.BemRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cruz-
 */
@Service
public class BemService {
    
    @Autowired
    private BemRepository bemRepository;
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Bem save (Bem bem){
        bemRepository.save(bem);
        return bem;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete (Long id){
        bemRepository.deleteById(id);
    }
    
    public Optional<Bem> findById (Long id){
        return bemRepository.findById(id);
    } 
    
    public Page<Bem> findAll(Pageable pageable){
        return bemRepository.findAll(pageable);
    } 

    
}
