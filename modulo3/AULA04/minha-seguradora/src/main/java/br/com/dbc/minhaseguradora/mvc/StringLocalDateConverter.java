package br.com.dbc.minhaseguradora.mvc;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author tiago
 */
public class StringLocalDateConverter implements Converter<String, LocalDate> {

    @Override
    public LocalDate convert(String sqlDate) {
        return (sqlDate == null ? null : LocalDate.parse(sqlDate, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }
}
