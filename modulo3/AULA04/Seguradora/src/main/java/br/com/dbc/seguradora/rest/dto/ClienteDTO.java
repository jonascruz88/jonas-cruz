/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.seguradora.rest.dto;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author tiago
 */
@Data
public class ClienteDTO implements Serializable{
    
    private String primeiroNome;
    private String ultimoNome;
    
}
