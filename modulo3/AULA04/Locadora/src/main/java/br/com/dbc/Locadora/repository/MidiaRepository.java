/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.repository;


import br.com.dbc.Locadora.entity.Midia;
import br.com.dbc.Locadora.entity.MidiaType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jonas.cruz
 */
public interface MidiaRepository extends JpaRepository<Midia, Long> {
    
    public long countByMidiaType(MidiaType midiaType);
    public Midia findByIdAluguel(Long id);
    public List<Midia> findByMidiaTypeAndIdFilmeId(MidiaType tipo, Long idFilme);
    public List<Midia> findByIdFilmeId(Long id);  
    public Midia findFirstByIdFilmeIdAndMidiaType(Long id, MidiaType tipo);
//    public List<Midia> findAllByIdFilmeId(Long id);
}
