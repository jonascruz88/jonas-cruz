/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.rest;

import br.com.dbc.Locadora.entity.Categoria;
import br.com.dbc.Locadora.entity.Midia;
import br.com.dbc.Locadora.entity.MidiaType;
import br.com.dbc.Locadora.service.MidiaService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author jonas.cruz
 */
@RestController
@RequestMapping("/api/midia")
//@PreAuthorize("hasAuthority('ADMIN_USER')")
public class MidiaRestController extends AbstractCrudRestController<Midia, Long, MidiaService>{

    @Autowired
    public MidiaService midiaService;
    
    @Override
    protected MidiaService getService() {
        return midiaService;
    }
    
    @GetMapping("/count/{midiaType}")
    public ResponseEntity<Long> countByMidiaType(@PathVariable MidiaType midiaType) {
       return ResponseEntity.ok(midiaService.countByMidiaType(midiaType));
    }
    
    @GetMapping("/search")
    public ResponseEntity<Page<Midia>> findByTituloIgnoreCaseOrCategoriaOrLancamento(
            Pageable pageable, @RequestParam(value = "titulo", defaultValue = "", required = false) String titulo,
            @RequestParam(value = "categoria", required = false) Categoria categoria,
            @RequestParam(value = "lancamentoIni", required = false) @DateTimeFormat(pattern="dd/MM/yyyy") 
                    LocalDate lancamentoIni,
            @RequestParam(value = "lancamentoFim", required = false) @DateTimeFormat(pattern="dd/MM/yyyy")
                    LocalDate lancamentoFim) {
       return ResponseEntity.ok(midiaService.findByTituloIgnoreCaseOrCategoriaOrLancamento(pageable, titulo, categoria, lancamentoIni, lancamentoFim));
    }

    
}
