/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.rest;

import br.com.dbc.Locadora.config.ConexaoCorreios;
import br.com.dbc.Locadora.entity.Cliente;
import br.com.dbc.Locadora.service.ClienteService;
import br.com.dbc.Locadora.ws.ConsultaCEP;
import br.com.dbc.Locadora.ws.ConsultaCEPResponse;
import br.com.dbc.Locadora.ws.ObjectFactory;
import javax.xml.bind.JAXBElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jonas.cruz
 */
@RestController
@RequestMapping("/api/cliente")
//@PreAuthorize("hasAuthority('ADMIN_USER')")
public class ClienteRestController extends AbstractCrudRestController<Cliente, Long, ClienteService> {
    

    @Autowired
    public ClienteService clienteService;

    @Override
    protected ClienteService getService() {
        return clienteService;
    }
    
    @Autowired ConexaoCorreios soapConnector;
    @Autowired ObjectFactory objectFactory;
    
    @GetMapping("/cep/{cep}")
    public ResponseEntity<?> cep(@PathVariable("cep") String cep) {
        
        return ResponseEntity.ok(clienteService.clienteComEndereco(cep));
    }

}
