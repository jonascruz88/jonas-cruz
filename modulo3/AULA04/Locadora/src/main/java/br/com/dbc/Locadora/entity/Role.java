package br.com.dbc.Locadora.entity;

import java.io.Serializable;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by nydiarra on 06/05/17.
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name="app_role")
public class Role extends AbstractEntity<Long> implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROLE_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "ROLE_SEQ", sequenceName = "ROLE_SEQ")
    private Long id;

    @Column(name="role_name")
    private String roleName;

    @Column(name="description")
    private String description;


}
