/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.service;

import br.com.dbc.Locadora.entity.Midia;
import br.com.dbc.Locadora.entity.ValorMidia;
import br.com.dbc.Locadora.repository.ValorMidiaRepository;
import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author jonas.cruz
 */
@Service
public class ValorMidiaService extends AbstractCrudService<ValorMidia, Long> {

    @Autowired
    public ValorMidiaRepository valorMidiaRepository;

    @Override
    protected JpaRepository<ValorMidia, Long> getRepository() {
        return valorMidiaRepository;
    }

    void deleteByIdMidiaId(Long id) {
        valorMidiaRepository.deleteByIdMidiaId(id);
    }

    void salvarPreco(List<Midia> midiasAtuais, double valor) {
        midiasAtuais.forEach(ma -> {
            ValorMidia valorMidia = valorMidiaRepository.findByIdMidiaIdAndFinalVigenciaIsNull(ma.getId())
                    .orElseGet(()->save(ValorMidia.builder()
                            .idMidia(ma)
                            .inicioVigencia(LocalDate.now())
                            .valor(valor)
                            .build()));
            if (!valorMidia.equals(valor)) {
                valorMidia.setFinalVigencia(LocalDate.now());
                save(valorMidia);
                save(ValorMidia.builder()
                        .idMidia(ma)
                        .inicioVigencia(LocalDate.now())
                        .valor(valor)
                        .build());
            }
        });
    }

    public List<ValorMidia> findByIdMidiaId(Long midiaId) {
        return valorMidiaRepository.findByIdMidiaId(midiaId);
    }

}
