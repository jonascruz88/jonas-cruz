/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.repository;

import br.com.dbc.Locadora.entity.ValorMidia;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jonas.cruz
 */
public interface ValorMidiaRepository extends JpaRepository<ValorMidia, Long> {

    public void deleteByIdMidiaId(Long id);
    public Optional<ValorMidia> findByIdMidiaIdAndFinalVigenciaIsNull(Long idMidia);
    public List<ValorMidia> findByIdMidiaId(Long midiaId);
    
}
