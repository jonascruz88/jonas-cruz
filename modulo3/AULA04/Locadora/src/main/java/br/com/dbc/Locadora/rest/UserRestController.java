/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.rest;

import br.com.dbc.Locadora.DTO.ChangePasswordDTO;
import br.com.dbc.Locadora.entity.User;
import br.com.dbc.Locadora.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jonas.cruz
 */
@RestController
@RequestMapping("/api/user")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class UserRestController extends AbstractCrudRestController<User, Long, UserService>{

    @Autowired
    private UserService userService;
    
    @Override
    protected UserService getService() {
        return userService;
    }
    
    @PostMapping("/password")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordDTO dto){
        return ResponseEntity.ok(userService.changePassword(dto));
    }
    
}
