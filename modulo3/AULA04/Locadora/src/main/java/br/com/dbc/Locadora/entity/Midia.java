/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas.cruz
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MIDIA")
public class Midia extends AbstractEntity<Long> implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MIDIA_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "MIDIA_SEQ", sequenceName = "MIDIA_SEQ")
    private Long id;

    @NotNull
    @Column(name = "MIDIA_TYPE")
    private MidiaType midiaType;
       
    @JoinColumn(name = "ID_ALUGUEL", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private Aluguel idAluguel;
    
    @JoinColumn(name = "ID_FILME", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Filme idFilme;

    
}
