/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.DTO;

import br.com.dbc.Locadora.entity.MidiaType;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas.cruz
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MidiaDTO implements Serializable {
    
    private MidiaType tipo;
    private int quantidade;
    private double valor;
}
