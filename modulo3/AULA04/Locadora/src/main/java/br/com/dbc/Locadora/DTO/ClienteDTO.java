/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas.cruz
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClienteDTO{ 
        
        private String rua;
        private String bairro;
        private String cidade;
        private String estado;

    
}
