/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.rest;

import br.com.dbc.Locadora.DTO.CatalogoDTO;
import br.com.dbc.Locadora.DTO.FilmeDTO;
import br.com.dbc.Locadora.DTO.TituloDTO;
import br.com.dbc.Locadora.entity.Categoria;
import br.com.dbc.Locadora.entity.Filme;
import br.com.dbc.Locadora.entity.ValorMidia;
import br.com.dbc.Locadora.service.FilmeService;
import br.com.dbc.Locadora.service.MidiaService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author jonas.cruz
 */
@RestController
@RequestMapping(path = "/api/filme")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class FilmeRestController extends AbstractCrudRestController<Filme, Long, FilmeService> {

    @Autowired
    public FilmeService filmeService;

    @Autowired
    public MidiaService midiaService;

    @Override
    protected FilmeService getService() {
        return filmeService;
    }

    @PostMapping("/midia")
    public ResponseEntity<?> salvarComMidia(@RequestBody FilmeDTO dto) {
        return ResponseEntity.ok(getService().saveFilmeDTO(dto));
    }

    @PutMapping("/{id}/midia")
    public ResponseEntity<?> upDateComMidia(@PathVariable("id") Long id, @RequestBody FilmeDTO dto) {
        return ResponseEntity.ok(getService().upDateComMidia(id, dto));
    }

    @GetMapping("/search")
    public ResponseEntity<Page<Filme>> findByTituloIgnoreCaseOrCategoriaOrLancamento(
            Pageable pageable, @RequestParam(value = "titulo", defaultValue = "", required = false) String titulo,
            @RequestParam(value = "categoria", required = false) Categoria categoria,
            @RequestParam(value = "lancamentoIni", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoIni,
            @RequestParam(value = "lancamentoFim", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoFim
    ) {
        return ResponseEntity.ok(filmeService.buscarFilmes(pageable, titulo, categoria, lancamentoIni, lancamentoFim));
    }

    @GetMapping("/precos/{id}")
    public ResponseEntity<Page<ValorMidia>> buscaPrecoFilme(Pageable pageable, @PathVariable("id") Long id) {
        return ResponseEntity.ok(filmeService.buscaPrecoFilme(pageable, id));
    }
    
    @PostMapping("/search/catalogo")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<Page<CatalogoDTO>> buscaPorTitulo(Pageable pageable, @RequestBody TituloDTO dto){
        return ResponseEntity.ok(filmeService.buscaPorTitulo(pageable, dto));
    }


}
