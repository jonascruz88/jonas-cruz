/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.service;

import br.com.dbc.Locadora.DTO.MidiaDTO;
import br.com.dbc.Locadora.entity.Categoria;
import br.com.dbc.Locadora.entity.Filme;
import br.com.dbc.Locadora.entity.Midia;
import br.com.dbc.Locadora.entity.MidiaType;
import br.com.dbc.Locadora.repository.MidiaRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author jonas.cruz
 */
@Service
public class MidiaService extends AbstractCrudService<Midia, Long> {

    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private FilmeService filmeService;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected JpaRepository<Midia, Long> getRepository() {
        return midiaRepository;
    }

    public long countByMidiaType(MidiaType midiaService) {
        return midiaRepository.countByMidiaType(midiaService);
    }

    public Midia findByIdAluguel(Long id) {
        return midiaRepository.findByIdAluguel(id);
    }
    
    public void salvarMidias(List<MidiaDTO> midia, Filme filme) {
        midia.forEach(m -> {
            List<Midia> midiasAtuais = midiaRepository.findByMidiaTypeAndIdFilmeId(m.getTipo(), filme.getId());
            if (midiasAtuais.size() < m.getQuantidade()) {
                int dif = m.getQuantidade() - midiasAtuais.size();
                for (int i = 0; i < dif; i++) {
                    midiaRepository.save(Midia
                            .builder()
                            .idFilme(filme)
                            .midiaType(m.getTipo())
                            .build());
                }
            } else if (midiasAtuais.size() > m.getQuantidade()) {
                int dif = midiasAtuais.size() - m.getQuantidade();
                midiasAtuais.stream()
                        .filter(ma -> ma.getIdAluguel() == null)
                        .limit(dif)
                        .forEach(ma -> {
                            valorMidiaService.deleteByIdMidiaId(ma.getId());
                            delete(ma.getId());
                        });
            }
            midiasAtuais = midiaRepository.findByMidiaTypeAndIdFilmeId(m.getTipo(), filme.getId());
            valorMidiaService.salvarPreco(midiasAtuais, m.getValor());
        });
    }
    
    public Page<Midia> findByTituloIgnoreCaseOrCategoriaOrLancamento(
            Pageable pageable,String titulo, Categoria categoria, LocalDate lancamentoIni, LocalDate lancamentoFim) {
        List<Midia> midias = new ArrayList<>();
        List<Midia> midiaByFilme = new ArrayList<>();
        List<Filme> listaFilmes = new ArrayList<>();
        lancamentoIni = lancamentoIni == null ? LocalDate.MIN : lancamentoIni;
        lancamentoFim = lancamentoFim == null ? LocalDate.MIN : lancamentoFim;
        Page<Filme> filmes = filmeService.buscarFilmes(
                pageable, titulo, categoria, lancamentoIni, lancamentoFim);
        listaFilmes = filmes.getContent();
        for(Filme filme : listaFilmes){
            midiaByFilme = midiaRepository.findByIdFilmeId(filme.getId());
            for(Midia midia : midiaByFilme){
                midias.add(midia);
            }
        }
       return  new PageImpl<>(midias, pageable, midias.size());
    }

    public List<Midia> findByIdFilmeId(Long id) {
        return midiaRepository.findByIdFilmeId(id);
    }
        
    public Midia findFirstByFilmeAndTipo(Long id, MidiaType tipo){
        return midiaRepository.findFirstByIdFilmeIdAndMidiaType(id, tipo);
    }

}
