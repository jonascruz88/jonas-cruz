/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.config;



import br.com.dbc.Locadora.ws.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 *
 * @author jonas.cruz
 */
public class ConexaoCorreios extends WebServiceGatewaySupport {
    
    private ConexaoCorreios conexaoCorreios;
    
    
    public ConexaoCorreios getConexao(){
        return conexaoCorreios;
    }
 
    public Object chamaWebService(String cep, Object request){
        return getWebServiceTemplate().marshalSendAndReceive(cep, request);
    }
}
