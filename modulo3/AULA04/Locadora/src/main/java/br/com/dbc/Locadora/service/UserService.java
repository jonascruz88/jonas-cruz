/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.service;

import br.com.dbc.Locadora.DTO.ChangePasswordDTO;
import br.com.dbc.Locadora.entity.User;
import br.com.dbc.Locadora.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author jonas.cruz
 */
@Service
public class UserService extends AbstractCrudService<User, Long> {
    

    @Autowired
    public UserRepository userRepository;


    protected JpaRepository<User, Long> getRepository() {
        return userRepository;
    }
    
    public User changePassword(ChangePasswordDTO dto){
        User c = userRepository.findByUsername(dto.getUsername());
        c.setPassword(dto.getPassword());
        return c;
    }
}
