/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.DTO;

import br.com.dbc.Locadora.entity.Categoria;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author jonas.cruz
 */
@Data
@Builder
@AllArgsConstructor
public class CatalogoDTO {
    
    private String titulo;
    private Categoria categoria;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate lancamento;
    private List<MidiaCatalogoDTO> midias;
    
}
