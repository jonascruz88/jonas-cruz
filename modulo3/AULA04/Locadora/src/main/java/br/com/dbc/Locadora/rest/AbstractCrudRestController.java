/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.rest;

import br.com.dbc.Locadora.entity.AbstractEntity;
import br.com.dbc.Locadora.service.AbstractCrudService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

/**
 *
 * @author jonas.cruz
 */

public abstract class AbstractCrudRestController<E extends AbstractEntity<ID>, ID, SERVICE extends AbstractCrudService<E, ID>> {
    
    protected abstract SERVICE getService();
    
    @GetMapping()
    public ResponseEntity<?> list(Pageable pageable) {
        return ResponseEntity.ok(getService().findAll(pageable));
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable ID id) {
        return getService().findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
    
    
    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable ID id, @RequestBody E entity) {
        return ResponseEntity.ok(getService().save(entity));
    }
    
    @PostMapping
    public ResponseEntity<?> post(@RequestBody E entity) {
        return ResponseEntity.ok(getService().save(entity));
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable ID id) {
        getService().delete(id);
        return ResponseEntity.noContent().build();
    }
    
    

    
}
