/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.service;

import br.com.dbc.Locadora.DTO.ClienteDTO;
import br.com.dbc.Locadora.config.ConexaoCorreios;
import br.com.dbc.Locadora.entity.Cliente;
import br.com.dbc.Locadora.repository.ClienteRepository;
import br.com.dbc.Locadora.ws.ConsultaCEP;
import br.com.dbc.Locadora.ws.ConsultaCEPResponse;
import br.com.dbc.Locadora.ws.ObjectFactory;
import javax.xml.bind.JAXBElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author jonas.cruz
 */
@Service
public class ClienteService extends AbstractCrudService<Cliente, Long>{
    
    @Autowired ConexaoCorreios soapConnector;
    @Autowired ObjectFactory objectFactory;

    @Autowired
    public ConexaoCorreios conexaoCorreios;
    
    @Autowired
    public ClienteRepository clienteRepository;

    @Override
    protected JpaRepository<Cliente, Long> getRepository() {
        return clienteRepository;
    }

    public ClienteDTO clienteComEndereco(String cep){
        ConsultaCEP buscaRequest = objectFactory.createConsultaCEP();
        buscaRequest.setCep(cep);
        ConsultaCEPResponse busca = ((JAXBElement<ConsultaCEPResponse>)soapConnector
                .chamaWebService(
                "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente", 
                        objectFactory.createConsultaCEP(buscaRequest))).getValue();
        ClienteDTO dto = ClienteDTO.builder()
                .rua(busca.getReturn().getEnd())
                .bairro(busca.getReturn().getBairro())
                .cidade(busca.getReturn().getCidade())
                .estado(busca.getReturn().getUf())
                .build();
        return dto;
    }
}
