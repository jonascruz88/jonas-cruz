/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.rest;

import br.com.dbc.Locadora.entity.ValorMidia;
import br.com.dbc.Locadora.service.ValorMidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jonas.cruz
 */
@RestController
@RequestMapping("/api/valormidia")
//@PreAuthorize("hasAuthority('ADMIN_USER')")
public class ValorMidiaRestController extends AbstractCrudRestController<ValorMidia, Long, ValorMidiaService> {

    @Autowired
    public ValorMidiaService valorMidiaService;
    
    @Override
    protected ValorMidiaService getService() {
        return valorMidiaService;
    }
    
        
}
