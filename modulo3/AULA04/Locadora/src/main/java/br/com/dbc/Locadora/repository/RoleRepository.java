package br.com.dbc.Locadora.repository;


import br.com.dbc.Locadora.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by nydiarra on 06/05/17.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
}
