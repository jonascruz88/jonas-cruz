/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.service;

import br.com.dbc.Locadora.DTO.CatalogoDTO;
import br.com.dbc.Locadora.DTO.FilmeDTO;
import br.com.dbc.Locadora.DTO.MidiaCatalogoDTO;
import br.com.dbc.Locadora.DTO.MidiaDTO;
import br.com.dbc.Locadora.DTO.TituloDTO;
import br.com.dbc.Locadora.entity.Categoria;
import br.com.dbc.Locadora.entity.Filme;
import br.com.dbc.Locadora.entity.Midia;
import br.com.dbc.Locadora.entity.MidiaType;
import br.com.dbc.Locadora.entity.ValorMidia;
import br.com.dbc.Locadora.repository.FilmeRepository;
import br.com.dbc.Locadora.repository.MidiaRepository;
import br.com.dbc.Locadora.repository.ValorMidiaRepository;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author jonas.cruz
 */
@Service
public class FilmeService extends AbstractCrudService<Filme, Long>{
    
    @Autowired
    public FilmeRepository filmeRepository;

    @Autowired
    public MidiaService midiaService;
    
    @Autowired
    public AluguelService aluguelService;


    @Autowired
    public ValorMidiaService valorMidiaService;

    
    @Autowired
    public MidiaRepository midiaRepository;
    
    @Autowired
    public ValorMidiaRepository valorMidiaRepository;

    protected JpaRepository<Filme, Long> getRepository() {
        return filmeRepository;
    }
    
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme saveFilmeDTO(@RequestBody FilmeDTO dto){
        Filme filme = Filme.builder().titulo(dto.getTitulo())
                .lancamento(dto.getLancamento())
                .categoria(dto.getCategoria())
                .build();
        getRepository().save(filme);
        for(MidiaDTO midiaDTO : dto.getMidia()){
            for (int i = 1; i <= midiaDTO.getQuantidade(); i++) {
                Midia midia = Midia.builder()
                        .midiaType(midiaDTO.getTipo())
                        .idFilme(filme)
                        .build();
                midiaRepository.save(midia);
                ValorMidia valorMidia = ValorMidia.builder()
                        .valor(midiaDTO.getValor())
                        .idMidia(midia)
                        .inicioVigencia(LocalDate.now())
                        .build();
                valorMidiaRepository.save(valorMidia);
            }
        }
        return filme;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme upDateComMidia(@RequestBody Long id, FilmeDTO dto){
        Filme filme  = filmeRepository.findById(dto.getId()).get();
        
        filme.setCategoria(dto.getCategoria());
        filme.setLancamento(dto.getLancamento());
        filme.setTitulo(dto.getTitulo());
        
        filmeRepository.save(filme);
        
        midiaService.salvarMidias(dto.getMidia(), filme);
        
        return filme;
    }
        

    
    public Page<Filme> buscarFilmes(
            Pageable pageable,String titulo, Categoria categoria, LocalDate lancamentoIni, LocalDate lancamentoFim) {        
        lancamentoIni = lancamentoIni == null ? LocalDate.MIN : lancamentoIni;
        lancamentoFim = lancamentoFim == null ? LocalDate.MIN : lancamentoFim;
       return filmeRepository.findByTituloContainingIgnoreCaseOrCategoriaOrLancamentoBetween(pageable, titulo, categoria, lancamentoIni, lancamentoFim);
    }
    
    public Page<ValorMidia> buscaPrecoFilme(Pageable pageable, Long id){         
        List<Midia> midias = midiaService.findByIdFilmeId(id);
        Long midiaId = midias.get(0).getId();
        List<ValorMidia> valores = valorMidiaService.findByIdMidiaId(midiaId);
        return new PageImpl<>(valores, pageable, valores.size());
    }
    
    public Page<CatalogoDTO> buscaPorTitulo(Pageable pageable, TituloDTO dto){
        List <Filme> filmes = filmeRepository.findByTituloIgnoreCaseContaining(dto.getTitulo());
        List <CatalogoDTO> catalogo = new ArrayList<>();
        for(Filme filme : filmes){
            List<Midia> listaMidias = new ArrayList<>();
            listaMidias.add(midiaService.findFirstByFilmeAndTipo(filme.getId(), MidiaType.DVD));
            listaMidias.add(midiaService.findFirstByFilmeAndTipo(filme.getId(), MidiaType.VHS));
            listaMidias.add(midiaService.findFirstByFilmeAndTipo(filme.getId(), MidiaType.BLUE_RAY));
            List <MidiaCatalogoDTO> listaMidiaDTO = new ArrayList<>();
            for (int i = 0 ; i < listaMidias.size(); i++){                
                MidiaCatalogoDTO midiaCatalogoDTO = MidiaCatalogoDTO.builder()
                        .tipo(listaMidias.get(i).getMidiaType())
                        .disponivel("Indisponível")
                        .valor(valorMidiaService.findById(listaMidias.get(i).getId()).get().getValor())
                        .build();
                if(listaMidias.get(i).getIdAluguel() == null){
                    midiaCatalogoDTO.setDisponivel("Disponível");
                    midiaCatalogoDTO.setPrevisaoEntrega("Não há");
                }
                else
                    midiaCatalogoDTO.setPrevisaoEntrega(listaMidias.get(i).getIdAluguel().getPrevisaoEntrega().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                listaMidiaDTO.add(midiaCatalogoDTO);
            }
            CatalogoDTO catDTO = CatalogoDTO.builder()
                    .titulo(filme.getTitulo())
                    .categoria(filme.getCategoria())
                    .lancamento(filme.getLancamento())
                    .midias(listaMidiaDTO)
                    .build();
            catalogo.add(catDTO);
        }
        return new PageImpl<>(catalogo, pageable, catalogo.size());
    }
}
