/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.DTO;

import java.util.List;
import lombok.Data;

/**
 *
 * @author cruz-
 */
@Data
public class AluguelDTO {
    
    
    private Long idCliente;
    private List<Long> midias;
    
}
