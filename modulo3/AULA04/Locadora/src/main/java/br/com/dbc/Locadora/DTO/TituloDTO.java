/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.DTO;

import lombok.Builder;
import lombok.Data;

/**
 *
 * @author jonas.cruz
 */
@Data
@Builder
public class TituloDTO {
    
    private String titulo;
    
}
