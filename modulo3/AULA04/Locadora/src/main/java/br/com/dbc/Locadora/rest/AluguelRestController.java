/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.rest;

import br.com.dbc.Locadora.DTO.AluguelDTO;
import br.com.dbc.Locadora.DTO.DevolucaoDTO;
import br.com.dbc.Locadora.entity.Aluguel;
import br.com.dbc.Locadora.entity.Filme;
import br.com.dbc.Locadora.service.AluguelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jonas.cruz
 */
@RestController
@RequestMapping("/api/aluguel")
//@PreAuthorize("hasAuthority('ADMIN_USER')")
public class AluguelRestController extends AbstractCrudRestController<Aluguel, Long, AluguelService>{
    
    @Autowired
    public AluguelService aluguelService;

    @Override
    protected AluguelService getService() {
        return aluguelService;
    }
    
    @PostMapping("/retirada")
    public ResponseEntity<?> retirada(@RequestBody AluguelDTO aluguelDTO) {
        if (aluguelDTO.getMidias() == null || aluguelDTO.getIdCliente() == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(aluguelService.retirar(aluguelDTO));
    }
    
    @GetMapping("/devolucao")
    public ResponseEntity<Page<Filme>> devolucaoHoje(Pageable pageable){
       return ResponseEntity.ok(aluguelService.entregaHoje(pageable));
    }
    
    @PostMapping("/devolucao")
    public ResponseEntity<?> devolucao(@RequestBody DevolucaoDTO devolucaoDTO) {
        return ResponseEntity.ok(aluguelService.devolver(devolucaoDTO));
    }
    
}
