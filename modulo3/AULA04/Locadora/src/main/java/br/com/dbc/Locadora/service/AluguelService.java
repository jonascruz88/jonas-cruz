/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.service;

import br.com.dbc.Locadora.DTO.AluguelDTO;
import br.com.dbc.Locadora.DTO.DevolucaoDTO;
import br.com.dbc.Locadora.entity.Aluguel;
import br.com.dbc.Locadora.entity.Filme;
import br.com.dbc.Locadora.repository.AluguelRepository;
import br.com.dbc.Locadora.repository.ClienteRepository;
import br.com.dbc.Locadora.repository.MidiaRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jonas.cruz
 */
@Service
public class AluguelService extends AbstractCrudService<Aluguel, Long> {

    @Autowired
    public AluguelRepository aluguelRepository;

    @Autowired
    public MidiaRepository midiaRepository;
  
    @Autowired
    public MidiaService midiaService;
    
    @Autowired
    public ValorMidiaService valorMidiaService;
    
    @Autowired
    public FilmeService filmeService;
    
    @Autowired
    public ClienteRepository clienteRepository;
       
    
    @Override
    protected JpaRepository<Aluguel, Long> getRepository() {
        return aluguelRepository;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Optional<Aluguel> retirar(AluguelDTO aluguelDTO) {
        LocalDate addDays = LocalDate.now();
        Aluguel aluguel = Aluguel.builder()
                .retirada(LocalDate.now())
                .previsaoEntrega(addDays)
                .idCliente(clienteRepository.findById(aluguelDTO.getIdCliente()).orElseGet(null))
                .build();
        getRepository().save(aluguel);
        for (Long id : aluguelDTO.getMidias()) {
            midiaService.findById(id).get().setIdAluguel(aluguel);
        }    
        return findById(aluguel.getId());
    }
    
    public Page<Filme> entregaHoje(Pageable pageable){
        List<Filme> filmes = new ArrayList();
        List<Aluguel> aluguelAux = buscaAluguel();
        for (Aluguel hoje : aluguelAux) {
            filmes.add(midiaService.findById(hoje.getId()).get().getIdFilme());
        }
        return new PageImpl<>(filmes, pageable, filmes.size());
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public double devolver(DevolucaoDTO devolucaoDTO) {
        double valorAPagar = 0;
        Long idAluguelAux = midiaService.findById(devolucaoDTO.getMidias().get(0)).get().getIdAluguel().getId();
        getRepository().findById(idAluguelAux).get().setDevolucao(LocalDateTime.now());
        if(getRepository().findById(idAluguelAux).get().getDevolucao().getHour() > 16){
            getRepository().findById(idAluguelAux).get().setMulta(1);
        }
        for (Long id : devolucaoDTO.getMidias()) { 
            midiaService.findById(id).get().setIdAluguel(null);
            valorAPagar += valorMidiaService.findById(id).get().getValor();
        }    
        return findById(idAluguelAux).get().getMulta() + valorAPagar;
    }
    
    public List<Aluguel> buscaAluguel(){
        return aluguelRepository.findByPrevisaoEntrega(LocalDate.now());
    }
    
}
