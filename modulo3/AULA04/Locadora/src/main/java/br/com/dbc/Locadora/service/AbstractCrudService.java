/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.service;


import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jonas.cruz
 */
@AllArgsConstructor
@Transactional(readOnly = true)
public abstract class AbstractCrudService <E, ID> {
    
    protected abstract JpaRepository<E, ID> getRepository();
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public E save (E entity){
        return getRepository().save(entity);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void edit (E entity){
        getRepository().save(entity);
    }
    
    public void delete (ID id){
        getRepository().findById(id);
    }
    
    public Optional<E> findById (ID id){
        return getRepository().findById(id);
    }
    
    public Page<E> findAll (Pageable pageable){
        return getRepository().findAll(pageable);
    }
    
}
