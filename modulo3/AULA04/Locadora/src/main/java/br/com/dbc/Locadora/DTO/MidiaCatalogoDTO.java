/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.DTO;

import br.com.dbc.Locadora.entity.MidiaType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author jonas.cruz
 */
@Data
@Builder
@AllArgsConstructor
public class MidiaCatalogoDTO {
    
    private MidiaType tipo;
    private double valor;
    private String disponivel;
    private String previsaoEntrega;
    
}
