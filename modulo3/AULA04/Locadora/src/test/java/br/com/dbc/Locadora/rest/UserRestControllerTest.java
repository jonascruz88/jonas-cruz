/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.rest;

import br.com.dbc.Locadora.DTO.ChangePasswordDTO;
import br.com.dbc.Locadora.LocadoraApplicationTests;
import br.com.dbc.Locadora.entity.User;
import br.com.dbc.Locadora.repository.UserRepository;
import br.com.dbc.Locadora.service.UserService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author jonas.cruz
 */
public class UserRestControllerTest extends LocadoraApplicationTests {
    
    @Autowired
    private UserRestController userRestController;
    
    @Autowired
    private UserRepository userRepository;  
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        User applicationUser = mock(User.class);
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(applicationUser);
        this.restMockMvc = MockMvcBuilders.standaloneSetup(userRestController)
                .setCustomArgumentResolvers(pageableArgumentResolver)
                .setMessageConverters(jacksonMessageConverter).build();
        userRepository.deleteAll();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getService method, of class UserRestController.
     */
    @Test
    public void criarUserTest() throws Exception{
        User user = User.builder().firstName("Miguel").lastName("Cruz").username("cruz.miguel").password("123456").build();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(user)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(user.getUsername()));
    }

    /**
     * Test of changePassword method, of class UserRestController.
     */
    //@Test
    @WithMockUser(username="john.doe", 
        password = "jwtpass", 
        authorities = {"STANDARD_USER"})
    public void testChangePassword() {
        System.out.println("changePassword");
        ChangePasswordDTO dto = ChangePasswordDTO.builder()
                .username("john.doe")
                .password("uhuhuhuh")
                .build();
        UserRestController instance = new UserRestController();
        String expResult = dto.getPassword();
        ResponseEntity result = instance.changePassword(dto);
        //assertEquals(expResult, result.getBody().toString().);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void testGetUsers() throws Exception {
        restMockMvc.perform(MockMvcRequestBuilders.get("/springjwt/users"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }
    
    @Test(expected = Exception.class)
    @WithMockUser(username="john.doe", 
            password = "jwtpass", 
            authorities = {"STANDARD_USER"})
    public void testGetUsersAccessDenied() throws Exception {
        restMockMvc.perform(MockMvcRequestBuilders.get("/springjwt/users"));
    }

    @Override
    protected AbstractCrudRestController getController() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
