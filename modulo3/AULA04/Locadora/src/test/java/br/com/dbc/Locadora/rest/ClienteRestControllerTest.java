/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.rest;

import br.com.dbc.Locadora.LocadoraApplicationTests;
import br.com.dbc.Locadora.entity.Cliente;
import br.com.dbc.Locadora.repository.ClienteRepository;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author jonas.cruz
 */
public class ClienteRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private ClienteRestController clienteRestController;

    @Autowired
    private ClienteRepository clienteRepository;

    
    protected AbstractCrudRestController getController() {
        return clienteRestController;
    }

    @Before
    public void beforeTest() {
        clienteRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void clienteCreateTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome")
                .telefone("9999")
                .rua("Galmendio Quadros")
                .numero("500")
                .bairro("Vicentina")
                .cidade("São Leopoldo")
                .estado("RS")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(c.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.numero").value(c.getNumero()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(c.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cidade").value(c.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value(c.getEstado()));
        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(c.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c.getBairro(), clientes.get(0).getBairro());
        Assert.assertEquals(c.getTelefone(), clientes.get(0).getTelefone());
    }
    
    @Test
    public void clienteUpdateTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome")
                .telefone("9999")
                .rua("Galmendio Quadros")
                .numero("500")
                .bairro("Vicentina")
                .cidade("São Leopoldo")
                .estado("RS")
                .build();
        clienteRepository.save(c);
        c.setNome("nome2");
        c.setTelefone("0000");
        restMockMvc.perform(MockMvcRequestBuilders.put("/api/cliente/{id}", c.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(c.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.numero").value(c.getNumero()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(c.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cidade").value(c.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value(c.getEstado()));
        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals("nome2", c.getNome());
        Assert.assertEquals(c.getBairro(), clientes.get(0).getBairro());
        Assert.assertEquals("0000", c.getTelefone());
    }
    
    


    

}
