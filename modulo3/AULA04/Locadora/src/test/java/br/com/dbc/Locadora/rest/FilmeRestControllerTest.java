/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.Locadora.rest;

import br.com.dbc.Locadora.DTO.CatalogoDTO;
import br.com.dbc.Locadora.DTO.FilmeDTO;
import br.com.dbc.Locadora.DTO.MidiaDTO;
import br.com.dbc.Locadora.DTO.TituloDTO;
import br.com.dbc.Locadora.LocadoraApplicationTests;
import br.com.dbc.Locadora.entity.Categoria;
import br.com.dbc.Locadora.entity.Filme;
import br.com.dbc.Locadora.entity.Midia;
import br.com.dbc.Locadora.entity.MidiaType;
import br.com.dbc.Locadora.entity.ValorMidia;
import br.com.dbc.Locadora.repository.FilmeRepository;
import br.com.dbc.Locadora.repository.MidiaRepository;
import br.com.dbc.Locadora.repository.ValorMidiaRepository;
import br.com.dbc.Locadora.service.FilmeService;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author jonas.cruz
 */
public class FilmeRestControllerTest extends LocadoraApplicationTests {
    
    private Pageable pageable;

    @Autowired
    private FilmeRestController filmeRestController;

    @Autowired
    private FilmeService filmeService;
    
    @Autowired
    private ValorMidiaRepository valorMidiaRepository;
    
    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private FilmeRepository filmeRepository;
    
    @Override
    protected AbstractCrudRestController getController() {
        return filmeRestController;
    }
    
    @Before
    public void beforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }


    @Test
        @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})

    public void filmeCreateTest() throws Exception {
        FilmeDTO f = FilmeDTO.builder()
                .titulo("titulo")
                .categoria(Categoria.AVENTURA)
                .lancamento(LocalDate.now())
                .midia(Arrays.asList(new MidiaDTO(MidiaType.VHS, 10, 10.50)))
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(f.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(f.getCategoria().toString()));
        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(f.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(f.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(f.getCategoria(), filmes.get(0).getCategoria());
    }
    
    @Test
        @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})

    public void filmeUpdateTest() throws Exception {
        Filme fe = filmeRepository.save(Filme.builder()
                .titulo("titulo")
                .categoria(Categoria.AVENTURA)
                .lancamento(LocalDate.now())
                .build());
        Midia mi = midiaRepository.save(Midia.builder()
                .midiaType(MidiaType.DVD)
                .idFilme(fe)
                .build());
        ValorMidia vmi = valorMidiaRepository.save(ValorMidia.builder()
                .valor(9.90)
                .inicioVigencia(LocalDate.now())
                .inicioVigencia(LocalDate.now())
                .idMidia(mi)
                .build());
        FilmeDTO f = FilmeDTO.builder()
                .id(fe.getId())
                .titulo("atualizado")
                .categoria(Categoria.ANIMACAO)
                .lancamento(LocalDate.now())
                .midia(Arrays.asList(new MidiaDTO(MidiaType.VHS, 10, 10.50)))
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/{id}/midia" , fe.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(f.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(f.getCategoria().toString()));
        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(f.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(f.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(f.getCategoria(), filmes.get(0).getCategoria());
    }
    
    @Test
        @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})

    public void filmePrecoTest() throws Exception {
        Filme fe = filmeRepository.save(Filme.builder()
                .titulo("titulo")
                .categoria(Categoria.AVENTURA)
                .lancamento(LocalDate.now())
                .build());
        Midia mi = midiaRepository.save(Midia.builder()
                .midiaType(MidiaType.DVD)
                .idFilme(fe)
                .build());
        ValorMidia vmi = valorMidiaRepository.save(ValorMidia.builder()
                .valor(9.90)
                .inicioVigencia(LocalDate.now())
                .inicioVigencia(LocalDate.now())
                .idMidia(mi)
                .build());
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/precos/{id}" , fe.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8))                
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].valor").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].inicioVigencia").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].finalVigencia").isEmpty());
        Assert.assertEquals(9.90, vmi.getValor(), 0.1);
    }
    
    @Test
        @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})

    public void filmeSearchTest() throws Exception {
        Filme filme = filmeRepository.save(Filme.builder()
                .titulo("titulo")
                .categoria(Categoria.AVENTURA)
                .lancamento(LocalDate.now())
                .build());
        Midia mi = midiaRepository.save(Midia.builder()
                .midiaType(MidiaType.DVD)
                .idFilme(filme)
                .build());
        ValorMidia vmi = valorMidiaRepository.save(ValorMidia.builder()
                .valor(9.90)
                .inicioVigencia(LocalDate.now())
                .inicioVigencia(LocalDate.now())
                .idMidia(mi)
                .build());
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/search", filme.getCategoria())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].titulo").value(filme.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].lancamento").value(filme.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].categoria").value(filme.getCategoria().toString()));;
                Page<Filme> fe = filmeService.buscarFilmes(pageable, "", filme.getCategoria(), LocalDate.MIN, LocalDate.MIN);
                Page<Filme> de = filmeService.buscarFilmes(pageable, "titulo", filme.getCategoria(), LocalDate.MIN, LocalDate.MIN);
        Assert.assertEquals(fe.getContent().get(0).getCategoria(), filme.getCategoria());
        Assert.assertEquals(de.getContent().get(0).getTitulo(), filme.getTitulo());
    }
    
    @Test
        @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void filmeCatalogoSearchTest() throws Exception{
        FilmeDTO filmeDTO = FilmeDTO.builder()
                            .titulo("João e o Feijão")
                            .lancamento(LocalDate.now())
                            .categoria(Categoria.AVENTURA)
                            .midia(Arrays.asList(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(1.90).build()                                   ))
                        .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)));             
        TituloDTO dto = TituloDTO.builder().titulo("oão").build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/search/catalogo")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(dto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value("João e o Feijão"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].categoria").value(Categoria.AVENTURA.name()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].lancamento").value(filmeDTO.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[0].tipo").value(MidiaType.DVD.name()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[0].valor").value(1.90))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midias.[0].disponivel").isNotEmpty());
    }




}
